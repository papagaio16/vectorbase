
/*(function($) {
    Drupal.behaviors.infoButton = {
        attach: function(){
            $('#info1').hide();
            $('#info2').hide();
            $(".infoButton").button()
                .width(130)
                .click(function(){
                $('#info1').slideToggle("fast");
                $('#info2').slideToggle("fast");

                return false
            });
        }
    };
})(jQuery);*/

(function($) {
    Drupal.behaviors.infoBootstrap = {
        attach: function(){
            $('#rowFluid').hide();
            $(".infoButton").button({ icons: { primary: "ui-icon-info", secondary: "ui-icon-triangle-1-s"}})
                .width(130)
                .click(function(){
                    var num = $(".span2").size();
                    /*if ( num > 6 ) {
                        alert(num);
                        $(".span2:eq(6)").addClass("span3");
                        $(".span3").removeClass("span2");
                        $(".span2:eq(6)").addClass("span4");
                        $(".span4").removeClass("span2");
                    }*/
                    $('#rowFluid').slideToggle("fast");
                    //$('#rowFluid').each().addClass("span8");
                    return false
                });
        }
    };
})(jQuery);


(function($) {
    Drupal.behaviors.downloadButton = {
        attach: function(){
            $('#filedwn').hide();
            $(".downloadButton").button({ icons: { primary: "ui-icon-circle-arrow-s", secondary: "ui-icon-triangle-1-s" } })
                .width(130)
                .addClass("lightGreen")
                .click(function(){
                $('#filedwn').slideToggle("fast")
                return false
            });
        }
    };
})(jQuery);

function createProviderFormFields(id, labelText,tooltip,regex) {
    var tr = '<tr>' ;
    // create a new textInputBox
    var textInputBox = $('<input />').attr({
        type: "text",
        id: id, name: id,
        title: tooltip
    });

    // create a new Label Text
    tr += '<td>' + labelText  + '</td>';
    tr += '<td>' + textInputBox + '</td>';
    tr +='</tr>';
    return tr;
}
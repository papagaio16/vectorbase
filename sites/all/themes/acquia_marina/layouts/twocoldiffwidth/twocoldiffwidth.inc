<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column different width'),
  'category' => t('Columns: 2'),
  'icon' => 'twocoldiffwidth.png',
  'theme' => 'twocoldiffwidth',
  'css' => 'twocoldiffwidth.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);

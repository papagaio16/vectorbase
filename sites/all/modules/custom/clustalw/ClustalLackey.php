<?php

use VectorBase\ToolHelpers\JobSpec;
require_once('/vectorbase/web/root/sites/all/modules/custom/tool_helpers/Lackey.php');

class ClustalLackey extends VectorBase\ToolHelpers\Lackey {
    
   
    protected function buildSubmitFiles(JobSpec $job_specification, $prefix){
        
        if (user_is_logged_in()) {
		$path_to_result_files = DRUPAL_ROOT . "/data/job_results/users";
	}	       
	else {
		$path_to_result_files = DRUPAL_ROOT . "/data/job_results/anonymous";
	}
        
        $path_to_submit_files = DRUPAL_ROOT . "/data/job_submit";
        # write single query file
        file_put_contents("$path_to_result_files/$prefix.query.fa",$job_specification->get_input());
        
        # create individual submit files
        $submit_str =   "universe        = vanilla\n" .
                        "executable      = /vectorbase/scripts/ClustalW.sh\n" .
                        "output          = $path_to_result_files/$prefix\n" .
                        "log             = $path_to_result_files/$prefix.log\n" .
                        "error           = $path_to_result_files/$prefix.results.err\n" .
                        "\n" .
                        "Arguments = -infile=$path_to_result_files/$prefix.query.fa ";

	dpm($job_specification->get_attributes());
        foreach ($job_specification->get_attributes() as $attrib => $value) {
            $submit_str .= $attrib . "=" . "$value ";
        }

        $submit_str .=  "\n\n" .
                        "Initialdir          = $path_to_result_files\n" .
                        "Transfer_executable = false\n" .
                        "Queue\n";

        $submit_filename = "$path_to_submit_files/$prefix" . ".condor";
        file_put_contents($submit_filename,$submit_str);
                
        return array("source /home/condor/software/condor.sh; condor_submit", $submit_filename);
    }

	public function getRunTime() {
		return 0;
	}
}


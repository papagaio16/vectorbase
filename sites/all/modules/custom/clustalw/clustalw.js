(function ($) {

    // making this function a jquery plugin so that it can be called within the module form ajax callback
    $.fn.scrollToElement = function(data) {
        $("html, body").animate({scrollTop: $('#' + data).offset().top-20}, "slow");
    };

    $(document).ready(function () {

        // This is a drupal function found in misc/ajax.js that we are overriding to load the popup dialog prior to submitting the AJAX form submit.
        Drupal.ajax.prototype.beforeSubmit = function(xmlhttprequest, options) {
            // Replacement code. Make sure this is the clustal form just to be safe.
            // this property is set by the drupal form api in the .module file.
            if( this.clustalSubmit ) {
                submitClustal();
            }
            if (this.deleteDatasetsClustalw) {
                alert("Datasets Deleted!\n(refresh the page to reflect the change)");
            }
            if (this.saveDatasetsClustalw) {
                alert("Datasets Saved!");
            }
            if (this.resetPageClustalw) {
                window.location.reload();
            }
        }

        // attempt to guess sequence type
        $("#edit-sequence").mouseout(function () {
            // lets make this easy: remove all the headers then see what we have left
            var fasta=$("#edit-sequence").val();
            var re = new RegExp("(>.*)","g");
            fasta=fasta.replace(re, "");

            // now if we only have atgcn-\s\r\n we've got ourselves some dna sequence
            var dna = /^[ACGTN\s\r\n-]+$/ig;

            // check the dna type if we have dna
            if(dna.test(fasta)){
                $('input:radio[name="type"]:[value="DNA"]').attr('checked',true);
            }
        });

        // if blast is injecting sequences, we should try to auto set the input type on page load
        //setTimeout(sample,2000)
        $("#edit-sequence").trigger("mouseout");


        // toggle scores div
        /*
         $("#toggleScores").live("click",function (event) {
         if( $("#scores").css('display')=='none'){
         $("#scores").css('display','block');
         $('#toggleScores').html("Hide Alignment Scores");
         }else{
         $("#scores").css('display','none');
         $('#toggleScores').html("View Alignment Scores");
         }
         });
         */

        // download scores
        $("#downloadScores").live("click",function (event) {
            //$.download(Drupal.settings.clustalw.clustalwPath+'/clustalwDownloadResults.php', 'id='+$("#jobId").text()+'&type=stdout' );
            $.download('tool_helpers/rest/'+$("#jobId").text()+'/clustalw_results', 'id='+$("#jobId").text()+'&type=stdout' );
	});

        // download scores
        $("#downloadTree").live("click",function (event) {
            //$.download(Drupal.settings.clustalw.clustalwPath+'/clustalwDownloadResults.php', 'id='+$("#jobId").text()+'&type=tree' );
            $.download('tool_helpers/rest/'+$("#jobId").text()+'/clustalw_results', 'id='+$("#jobId").text()+'&type=tree' );
        });

        // download results
        $("#download").live("click",function (event) {
            //$.download(Drupal.settings.clustalw.clustalwPath+'/clustalwDownloadResults.php', 'id='+$("#jobId").text()+'&type=align' );
            $.download('tool_helpers/rest/'+$("#jobId").text()+'/clustalw_results', 'id='+$("#jobId").text()+'&type=align' );
        });

        // send results to hmmer
        $("#sendToHMMer").live("click",function (event) {
            $.customPost('/hmmer', 'id='+$("#jobId").text() );
        });


        jQuery.customPost = function(url, data){
            //url and data options required
            if( url && data ){
                //data can be string of parameters or array/object
                data = typeof data == 'string' ? data : jQuery.param(data);
                //split params into form inputs
                var inputs = '';
                jQuery.each(data.split('&'), function(){
                    var pair = this.split('=');
                    inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />';
                });
                //send request
                jQuery('<form action="'+ url +'" method="post" target="_blank">'+inputs+'</form>')
                    .appendTo('body').submit().remove();
            };
        };

        jQuery.download = function(url, data, method){
            //url and data options required
            if( url && data ){
                //data can be string of parameters or array/object
                data = typeof data == 'string' ? data : jQuery.param(data);
                //split params into form inputs
                var inputs = '';
                jQuery.each(data.split('&'), function(){
                    var pair = this.split('=');
                    inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />';
                });
                //send request
                jQuery('<form action="'+ url +'" method="'+ (method||'post') +'">'+inputs+'</form>')
                    .appendTo('body').submit().remove();
            };
        };

        /**********************************************
         job has been submitted. we have an id returned.
         make pop up and wait until we have some job results
         */



        var statusRepeat;
        var jobId;
        var isRaw=true;
        var pinwheel='<br/><img src="'+Drupal.settings.clustalw.clustalwPath+'/ajax-loader.gif">';

        function submitClustal() {

            // create dialog popup
            $("#submissionDialog").dialog({
                autoOpen: true,
                show: "scale",
                hide: "scale",
                width: 300,
                height: 200,
                draggable: false,
                modal: true,
                title:"ClustalW Job Status"
            });

            if($('#edit-lookup').val()==''){
                $("#submissionDialog").html('Submitting job'+pinwheel);
            }else{
                $("#submissionDialog").html('Looking up job'+pinwheel);
            }
        }

        // job id element has changed and the new value is presumably a new job id
        $('#edit-jobid').bind('DOMNodeInserted DOMNodeRemoved', function(event) {
            if (event.type == 'DOMNodeInserted' && $('#edit-jobid').text()!='') {
                parseId=$('#parseJobId').text();
                jobId=parseId;

                $("#submissionDialog").dialog("open");
                $("#submissionDialog").html('Job '+parseId+' is running'+pinwheel);
                // keep checking status until we're all done
                getJobStatus();
            }
        });	//end edit-jobid has changed


        function getJobStatus() {

            $.ajax({
                type: "GET",
                url: "/tool_helpers/rest/" + jobId + "/wait",
                timeout: 3200000,
                success: function(status) {
                    $.ajax({
                        type: "POST",
                        //url: Drupal.settings.clustalw.clustalwPath + "/displayResults.php",
                        url: "/tool_helpers/rest/" + jobId + "/clustalw_results",
                        data: "id=" + jobId + "&type=full&ieIsCrap=" + Math.round(new Date().getTime() / 1000.0),
                        success: function(msg) {
                            $("#edit-result").html(msg);
                            $("#submissionDialog").dialog("close");
                            $().scrollToElement("edit-result"); //scroll to the results element if the job retriev    al was successful.
                            $("#edit-jobid").html('');
                        },
                        error: function(msg) {
                            $("#submissionDialog").dialog("open");
                            $("#submissionDialog").html('Job ' + jobId + ' encountered an error while parsi    ng results: ' + msg.responseText);
                        }
                    });


                },
                error: function(msg) {
                    $("#submissionDialog").dialog("open");
                    $("#submissionDialog").html("Error: " + msg.responseText);
                }
            });
        }
        /*
         end of job submission handling
         **********************************************/

        //An event handler used to toggle the different fieldsets and fields(solves drupal_command_invoke problem)
        jQuery('#edit-alignment-full').bind('click', function() {
            jQuery('#edit-fast').hide();
            jQuery('#edit-full').show();
        });

        jQuery('#edit-alignment-fast').bind('click', function() {
            jQuery('#edit-full').hide();
            jQuery('#edit-fast').show();
        });

        /* Left here just in case they are going to be implemented in the future
         jQuery('#edit-type-dna').bind('click', function() {

         });

         jQuery('#edit-type-protein').bind('click', function() {

         });
         **********************************************************************/
    });
})(jQuery);

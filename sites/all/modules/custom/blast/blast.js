(function($) {

    // making this function a jquery plugin so that it can be called within the module form ajax callback
    $.fn.scrollToElement = function(data) {
        $("html, body").animate({scrollTop: $('#' + data).offset().top - 20}, "slow");
    };



    $(document).ready(function() {

        // This is a drupal function found in misc/ajax.js that we are overriding to load the popup dialog prior to submitting the AJAX form submit.
        Drupal.ajax.prototype.beforeSubmit = function(xmlhttprequest, options) {
            // Replacement code. Make sure this is the blast form just in case.
            // this property is set by the drupal form api in the .module file.
            if (this.blastSubmit) {
                submitBlast();
            }
            if (this.deleteDatasets) {
                alert("Datasets Deleted!\n(refresh the page to reflect the change)");
            }
            if (this.saveDatasets) {
                alert("Datasets Saved!");
            }
            if (this.resetPage) {
                window.location.reload();
            }

        }

        // weird firefox bug.
        $("#edit-sequence").css("display", 'inline-block');

        // stupid ie ajax caching bug
        $.ajaxSetup({cache: false});

        //$('#edit-complexitymasking').val('Default');
        //console.log($('#edit-complexitymasking').val());
        //$('#edit-complexitymasking select>option:eq(1)').attr('selected', true);
        //console.log($('#edit-complexitymasking').val());

        // blast program descriptions for labels and their radio buttons
        $("label[for='edit-program-blastn']").mouseover(function() {
            $('#blastProgramDescription').text('blastn - Nucleotide vs. Nucleotide');
        });
        $("label[for='edit-program-tblastn']").mouseover(function() {
            $('#blastProgramDescription').text('tblastn - Peptide vs. Translated Nucleotide');
        });
        $("label[for='edit-program-tblastx']").mouseover(function() {
            $('#blastProgramDescription').text('tblastx - Translated Nucleotide vs. Translated Nucleotide');
        });
        $("label[for='edit-program-blastp']").mouseover(function() {
            $('#blastProgramDescription').text('blastp - Peptide vs. Peptide');
        });
        $("label[for='edit-program-blastx']").mouseover(function() {
            $('#blastProgramDescription').text('blastx - Translated Nucleotide vs. Peptide');
        });
        $("input[name='program'][value='blastn']").mouseover(function() {
            $('#blastProgramDescription').text('blastn - Nucleotide vs. Nucleotide');
        });
        $("input[name='program'][value='tblastn']").mouseover(function() {
            $('#blastProgramDescription').text('tblastn - Peptide vs. Translated Nucleotide');
        });
        $("input[name='program'][value='tblastx']").mouseover(function() {
            $('#blastProgramDescription').text('tblastx - Translated Nucleotide vs. Translated Nucleotide');
        });
        $("input[name='program'][value='blastp']").mouseover(function() {
            $('#blastProgramDescription').text('blastp - Peptide vs. Peptide');
        });
        $("input[name='program'][value='blastx']").mouseover(function() {
            $('#blastProgramDescription').text('blastx - Translated Nucleotide vs. Peptide');
        });


        // run the checks when program changes
        $("input[name='program']", '#blast-ajax-form').change(function() {
            blastProgramChecks();
        });

        // blast program checks
        function blastProgramChecks() {
            var checked = $("input[name='program']:checked", '#blast-ajax-form').val();

            if(Drupal.settings.datasetExists == false){
                setDefaultWordSize(checked);
                setDefaultComplexityMasking(checked);
            }

            // hide scoring matrix for blastn since it isn't supported in new blastall
            if (checked == 'blastn') {
                $("label[for='edit-scoringmatrix']").css("display", "none");
                $("#edit-scoringmatrix").css("display", "none");
                $("#edit-scoringmatrix").attr("disabled", "disabled");
            } else {
                $("label[for='edit-scoringmatrix']").css("display", "block");
                $("#edit-scoringmatrix").css("display", "block");
                $("#edit-scoringmatrix").removeAttr("disabled");
            }
            setAvailableDbs(checked);
            setCheckedDbs();
        }

        // set default wordsize to 11 for blastn, 3 otherwise
        function setDefaultWordSize(checked) {
            if (checked == 'blastn') {
                $("#edit-wordsize").val('11').attr("selected", "selected");
            } else {
                $("#edit-wordsize").val('3').attr("selected", "selected");
            }
        }

        // set low complexity masking off for blastp
        function setDefaultComplexityMasking(checked) {
            if (checked == 'blastp') {
                $("#edit-complexitymasking").val('no').attr("selected", "selected");
            } else {
                $("#edit-complexitymasking").val('Default').attr("selected", "selected");
            }
        }

        // set available dbs for the selected program
        function setAvailableDbs(checked) {
            if (checked == 'blastp' || checked == 'blastx') {
                // only pep dbs
                // uncheck and disable nucs
                $("[id^=edit-nucleotide] .nucDbs").removeAttr("checked");
                $("[id^=edit-nucleotide]").attr("disabled", "disabled").addClass("disabledFields");
                // enable peps
                $("[id^=edit-peptide]").removeAttr("disabled").removeClass("disabledFields");
            } else {
                // only nuc dbs
                // uncheck and disable peps
                $("[id^=edit-peptide] .pepDbs").attr("checked", false);
                $("[id^=edit-peptide]").attr("disabled", "disabled").addClass("disabledFields");
                // enable nucs
                $("[id^=edit-nucleotide]").removeAttr("disabled").removeClass("disabledFields");
            }
        }


        // datasets: toggle active dataset displayed
        $(".organismCheckboxDiv").mouseover(function() {
            // clear all visible sets
            $(".dbContainer:not([data-org=" + $(this).attr('data-org') + "])").css("display", "none");
            // enable selected set
            $(".dbContainer[data-org=" + $(this).attr('data-org') + "]").css("display", "block");

            //clear all highlighted organism name divs
            $(".organismCheckboxDiv").stop().animate({boxShadow: "#eee 0px 0px 0px 0px"}, 600);

            // highlight this div
            $(this).stop().animate({boxShadow: "#ddd 0px 0px 7px 8px"}, 300);
        });


        // datasets: check associated dbs toggle
        $(".organismCheckbox").live("click", function(event) {
            if(this.checked == false){
                $(".allTypeSelector[data-type=" + $(this).attr('data-type') + "]:checkbox" ).attr('checked', this.checked);
                $(".allDatasets:checkbox" ).attr('checked', this.checked);
            }
            $(".dbs[data-org=" + $(this).attr('data-org') + "]:checkbox").attr('checked', this.checked);
            //uncheck bac dbs
            $(".dbs[data-org=" + $(this).attr('data-org') + "].bac:checkbox").removeAttr('checked');
            setAvailableDbs($("input[name='program']:checked", '#blast-ajax-form').val());
        });

        // examine all checked datasets, check dbs according to what program is selected
        function setCheckedDbs() {
            $(".organismCheckbox").trigger('change');
            if ($(".allDatasets").attr('checked')) {
                if ($(".allSelector.pepDbs").is(':disabled')) {
                    $(".allSelector.nucDbs").attr('checked', 'true');
                }
                else {
                    $(".allSelector.pepDbs").attr('checked', 'true');
                }
            }
        }

        // master toggle for all datasets
        $(".allDatasets").live("click", function(event) {
            $(".organismCheckboxDiv :checkbox").attr('checked', this.checked);
            $(".dbs:checkbox").attr('checked', this.checked);

            // calling this function is taking forever. lets speed it up
            setAvailableDbs($("input[name='program']:checked", '#blast-ajax-form').val());
        });


        // toggles for specific type select alls
        $(".allSelector").live("click", function(event) {
            var thisType = $(this).attr('data-type');
            $("label.option:contains('" + thisType + "')").each(function(i, v) {
                var idOfCheckbox = $(v).attr('for');
                $("#" + idOfCheckbox).attr('checked', $(".allSelector[data-type=" + thisType + "]").attr('checked'));
            });

        });

        $(".allTypeSelector").live("click", function(event) {
            if(this.checked == false){
                $(".allDatasets:checkbox" ).attr('checked', this.checked);
            }         var thisType = $(this).attr('data-type');
            $("label.option:contains('" + thisType + "')").each(function(i, v) {
                var idOfCheckbox = $(v).attr('for');
                $("#" + idOfCheckbox).attr('checked', $(".allTypeSelector[data-type=" + thisType + "]").attr('checked'));
            });
            $(".dbs[data-type=" + $(this).attr('data-type') + "]:checkbox").attr('checked', this.checked);
            //uncheck bac dbs
            $(".dbs[data-type=" + $(this).attr('data-type') + "].bac:checkbox").removeAttr('checked');
            $(".allTypeSelectorDbs[data-org=" + $(this).attr('data-org') + "]:checkbox").attr('checked', this.checked);
            setAvailableDbs($("input[name='program']:checked", '#blast-ajax-form').val());

        });

        //if any type dbs are checked also check all other types
        $(".allTypeSelectorDbs").live("click", function(event){
            var thisType = $(this).attr('data-type');
            var thisOrg = $(this).attr('data-org');

            //check parent all dataset
            if ($(".allTypeSelectorDbs[data-org=" + thisOrg + "]:checked").length > 0 ) {
                $(".allTypeSelector[data-org=" + thisOrg + "]:checkbox").attr('checked',true);
            } else{
                $(".allTypeSelector[data-org=" + thisOrg + "]:checkbox").removeAttr('checked');
            }

            //check individual databases and organisms
            $("label.option:contains('" + thisType + "')").each(function(i, v) {
                var idOfCheckbox = $(v).attr('for');
                if($("#" + idOfCheckbox).attr('data-type') == thisOrg) $("#" + idOfCheckbox).attr('checked', $(".allTypeSelectorDbs[data-nameType=" + thisOrg + thisType + "]").attr('checked'));

                if($("#" + idOfCheckbox).attr('data-type') == thisOrg){
                    var thatOrg = $("#" + idOfCheckbox).attr('data-org');
                    if($(".dbs[data-org=" + thatOrg + "]:checked").length > 0){
                        $(".organismCheckbox[data-org=" + thatOrg + "]:checkbox").attr('checked', true);
                    } else {
                        $(".organismCheckbox[data-org=" + thatOrg + "]:checkbox").removeAttr('checked');
                    }

                }

            });

        });
        // if any dbs are checked, also check it's parent dataset
        $(".dbs").live("click", function(event) {
            if ($(".dbs[data-org=" + $(this).attr('data-org') + "]:checked").length > 0) {
                $(".organismCheckbox[data-org=" + $(this).attr('data-org') + "]:checkbox").attr('checked', true);
            } else {
                $(".organismCheckbox[data-org=" + $(this).attr('data-org') + "]:checkbox").removeAttr('checked');
            }
        });



        /**********************************************
         job has been submitted. we have an id returned.
         make pop up and wait until we have some job results
         */



        var statusRepeat;
        var jobId;
        var isRaw = true;
        var pinwheel = '<br/><img src="' + Drupal.settings.blast.blastPath + '/ajax-loader.gif">';


        function submitBlast() {
            // create dialog popup
            $("#submissionDialog").dialog({
                autoOpen: true,
                show: "scale",
                hide: "scale",
                width: 270,
                height: 100,
                draggable: false,
                modal: true,
                title: "BLAST Job Status"
            });


            if($('#condor-id').val()) {
                $("#submissionDialog").html('Your job has been submitted.<br>' + $('#condor-id').text());
            } else if ($('#edit-lookup').val() == '') {
                $("#submissionDialog").html('Submitting job' + pinwheel);
            } else {
                $("#submissionDialog").html('Looking up job' + pinwheel);
            }
        }

// job id element has changed and the new value is presumably a new job id
        $('#edit-jobid').bind('DOMNodeInserted DOMNodeRemoved', function(event) {
            if (event.type == 'DOMNodeInserted' && $('#edit-jobid').text() != '') {

                /*if($('#rawJobId')) {
                 rawId = $('#rawJobId').text();
                 parseId = $('#parseJobId').text();
                 jobId = rawId;
                 } else {*/
                jobId = $('#condor-id').text();
                //}

                $("#submissionDialog").dialog("open");
                $("#submissionDialog").html('Job ' + jobId + ' is running' + pinwheel);
                // keep checking status until we're all done
                getJobStatus();

            }
        });	//end edit-jobid has changed


        function getJobStatus() {

            $.ajax({
                type: "GET",
                url: "/tool_helpers/rest/" + jobId + "/wait",
                timeout: 3200000,
                success: function(status) {
                    $.ajax({
                        type: "POST",
                        url: Drupal.settings.blast.blastPath + "/displayResults.php",
                        data: "id=" + jobId + "&ieIsCrap=" + Math.round(new Date().getTime() / 1000.0),
                        success: function(msg) {
                            $("#edit-result").html(msg);
                            $("#submissionDialog").dialog("close");
                            $().scrollToElement("edit-result"); //scroll to the results element if the job retrieval was successful.
                            $("#edit-jobid").html('');
                            loadInputParams(msg);
                        },
                        error: function(msg) {
                            $("#submissionDialog").dialog("open");
                            $("#submissionDialog").html('Job ' + jobId + ' encountered an error while parsing results: ' + msg.responseText);
                        }
                    });


                },
                error: function(msg) {
                    $("#submissionDialog").dialog("open");
                    $("#submissionDialog").html("Error: " + msg.responseText);
                }
            });
        }

        /*
         end of job submission handling
         **********************************************/




        /****************************
         for parsing of job input params
         ******************************************/
        function returnOneSubstring(regex, input) {
            regex.exec(input);
            return RegExp.$1;
        }

        function loadInputParams(input) {
            //var l1 = new Date().getTime();
            // load job input parameters
            $("#edit-sequence").val(returnOneSubstring(/sequence=([\s\S]*?)IIIjustInCase;/, input));

            if (returnOneSubstring(/description=([\s\S]*?);/m, input) != '') {
                $("#edit-description").val(returnOneSubstring(/description=([\s\S]*?);/m, input));
            }
            if (returnOneSubstring(/maxEvalue=([\w|\d|-]+);/m, input) != '') {
                $("#edit-maxevalue").val(returnOneSubstring(/maxEvalue=([\w|\d|-]+)/m, input));
            }
            if (returnOneSubstring(/wordSize=([\d]+);/m, input) != '') {
                $("#edit-wordsize").val(returnOneSubstring(/wordSize=([\d]+)/m, input));
            }
            if (returnOneSubstring(/complexityMasking=([\w]+);/m, input) != '') {
                $("#edit-complexitymasking").val(returnOneSubstring(/complexityMasking=([\w]+)/m, input));
            }
            if (returnOneSubstring(/num_alignments=([\d]+);/m, input) != '') {
                $("#edit-numberofresults").val(returnOneSubstring(/num_alignments=([\d]+)/m, input));
            }
            if (returnOneSubstring(/scoringMatrix=([\w|\d]+)/m, input) != '') {
                $("#edit-scoringmatrix").val(returnOneSubstring(/scoringMatrix=([\w|\d]+)/m, input));
            }

            var program = returnOneSubstring(/program=([\w]+)/m, input);
            if (program != '') {
                //$("#edit-program").val(program);
                var programRadio = $("input[name='program']").filter('[value=' + program + ']').attr('checked', true);
            }

            //var l2 = new Date().getTime();
            //console.log('Time took for job input param settings to get filled in: ' + (l2 - l1));

            // run some checks on this new data we're importing to the form
            // hide scoring matrix for blastn since it isn't supported in new blastall
            if (program == 'blastn') {
                $("#edit-scoringmatrix").css("display", "none");
                $("#edit-scoringmatrix").attr("disabled", "disabled");
            } else {
                $("#edit-scoringmatrix").css("display", "block");
                $("#edit-scoringmatrix").removeAttr("disabled");
            }

            //var l3 = new Date().getTime();
            //console.log('Time took for some more css and attr modifications: ' + (l3 - l2));

            //  --- check dbs ---
            // first, uncheck all datasets/dbs currently selected
            $(".organismCheckboxDiv :checkbox").removeAttr('checked');
            $(".dbs:checkbox").removeAttr('checked');


            // check dbs listed in inputParams
            var pattern = /database\d+=([\w|.|-]+);/mg;
            var match;
            var org;
            while ((match = pattern.exec(input)) != null) {
                // check this db
                $(".dbs[data-filename=" + match[1] + "]:checkbox").attr('checked', 'checked');
                // what is data-org for this db?
                org = $(".dbs[data-filename=" + match[1] + "]:checkbox").data("org");
                // check that parent dataset this db belongs to
                $(".organismCheckbox[data-org='" + org + "']:checkbox").attr('checked', 'checked');
            }



            //var l4 = new Date().getTime();
            //console.log('Time took for db check box fill-ins: ' + (l4 - l3));
            // do checks on dbs and select datasets of selected dbs
            setAvailableDbs(program);
            //var l5 = new Date().getTime();
            //console.log('Time took for setAvailableDbs to run: ' + (l5 - l4));
            setCheckedDbs();
            //var l6 = new Date().getTime();
            //console.log('Time took for setCheckedDbs to run: ' + (l6 - l5));
        }




// set some images for the sortable table columns
        $('th.headerSortUp').css('background-image', "url('/" + Drupal.settings.blast.blastPath + "/asc.gif')");
        $('th.headerSortDown').css('background-image', "url('/" + Drupal.settings.blast.blastPath + "/desc.gif')");


// initially sort the top level results table on most hits, then org name, then db name
        $('#edit-result').bind('DOMNodeInserted DOMNodeRemoved', function(event) {
            if (event.type == 'DOMNodeInserted') {
                // if there is red error text in here, we need to close the submission/status popup
                if ($(this).css('color') == 'rgb(255, 0, 0)') {
                    $("#submissionDialog").dialog("close");
                }

                if ($("#topLevelTable").attr("data-initial") == "sortme") {
                    //console.log( 'declaring sort order' );
                    $("#topLevelTable").removeAttr('data-initial');
                    // declare our results table as sortable
                    $("#topLevelTable").tablesorter({
                        // sort on the third column (desc), then first column (asc), then 2nd(asc)
                        sortList: [[2, 1], [0, 0], [1, 0]]
                    });
                }

            }
        });

// load hsp results through ajax
        $(".dbResult").live("click", function(event) {
            var dataDb = $(this).attr("data-db");
            var dataType = $(this).attr("data-type");
            // remove highlighting of previously checked dbs
            $(".dbResult").stop().animate({boxShadow: "#eee 0px 0px 0px 0px"}, 600);
            //highlight this db
            $(this).stop().animate({boxShadow: "#ddd 0px 0px 7px 8px"}, 300);


            //console.log( Drupal.settings.blast.blastPath+" id=" + $(this).attr('data-id')+"&db="+$(this).attr('data-db') );
            $("#hspLevelResults").html('Retrieving results<br/><img src="' + Drupal.settings.blast.blastPath + '/ajax-loader.gif">');

            $.ajax({
                type: "POST",
                data: "id=" + $(this).attr('data-id') + "&db=" + $(this).attr('data-db') + "&data-type=" + $(this).attr('data-type'),
                url: Drupal.settings.blast.blastPath + "/blastHspResults.php",
                success: function(msg) {
                    $("#hspLevelResults").html(msg)
                    $("#hspLevelResults").data("data-db", dataDb);
                    $("#hspLevelResults").data("data-type", dataType);
// declare our results table as sortable
                    switch(dataType) {
                        case "Chromosomes":
                        case "Contigs":
                        case "Scaffolds":
                            $(".hspHitDesc").hide();
                            break;
                        default:
                            $(".hspHitDesc").show();

                    }
                    $("#blastHsps").tablesorter({
                        headers: {
// disable sorting on checkbox column (we start counting zero)
                            0: {
                                sorter: false
                            },
// hit names are links but sort on the text inside the link
                            1: {
                                sorter: 'links'
                            },
// special case for evalue column
                            3: {
                                sorter: 'scinot'
                            }
                        },
// Here is where you should change the sorting by e-value
                        sortList: [[6, 1]]
                    });

//scroll view down to where results start
                    var scrollTo = $('#blastHsps'),
                        container = $('html, body');

                    var scroll = scrollTo.offset().top - container.offset().top + container.scrollTop();
                    scroll = scrollTo.offset().top - 400;
                    container.animate({
                        scrollTop: scroll,
                    }, "slow");

                }
            });
        });


// set defualt for animation speeds
        $.fx.speeds._default = 350;

// load popups with hsp details
        $(".hsp").live("click", function(event) {

            // conifg the dialog popups
            $("#hspDialog").dialog({
                autoOpen: false,
                show: "scale",
                hide: "scale",
                position: "center",
                width: 750,
                height: 600,
                maxWidth: 800,
                maxHeight: 1200,
                draggable: true,
                modal: true,
            });

            $("#hspDialog").dialog('open');
            $("#hspDialog").html('Retrieving HSP details<br/><img src="' + Drupal.settings.blast.blastPath + '/ajax-loader.gif">');
            $("#hspDialog").dialog('option', 'title', "BLAST HSP Details");

            $.ajax({
                type: "POST",
                data: "id=" + $(this).attr('id'),
                url: Drupal.settings.blast.blastPath + "/hspDetails.php",
                success: function(msg) {
                    $("#hspDialog").html(msg);
//$("#hspDialog").title("HSP Result");
                    $("#hspDialog").dialog('open');
                }
            });
        }); // end .hsp click events


// allow clicks in grayed out area to close the hsp details dialog
        $('.ui-widget-overlay').live("click", function() {
            //Close the dialog
            $("#hspDialog").dialog("close");
        });

// master toggle for hsp results
        $("#hspsMaster").live("click", function(event) {
            $("#blastHsps .hsps").attr('checked', this.checked);
        });

// toggle query/hit graphics and numbers
        $("#hspGraphicTextSwitch").live(
            "click",
            function() {
                var showGraphics = $('#hspGraphicTextSwitch').text() === 'Show Query/Hit Graphics';
                // initial state is graphic
                var qG = $('#queryGraphic').index();
                var hG = $('#hitGraphic').index();
                var qS = $('#queryStart').index();
                var qE = $('#queryEnd').index();
                var hS = $('#hitStart').index();
                var hE = $('#hitEnd').index();

                if (showGraphics) {
                    $('th[id=\'queryGraphic\']').show();
                    $('th[id=\'hitGraphic\']').show();
                    $('th[id=\'queryStart\']').hide();
                    $('th[id=\'queryEnd\']').hide();
                    $('th[id=\'hitStart\']').hide();
                    $('th[id=\'hitEnd\']').hide();
                } else {
                    $('th[id=\'queryGraphic\']').hide();
                    $('th[id=\'hitGraphic\']').hide();
                    $('th[id=\'queryStart\']').show();
                    $('th[id=\'queryEnd\']').show();
                    $('th[id=\'hitStart\']').show();
                    $('th[id=\'hitEnd\']').show();
                }

                $('table[id=\'blastHsps\'] tbody tr').each(
                    function() {
                        if (showGraphics) {
                            $(this).find('td').eq(qG).show();
                            $(this).find('td').eq(hG).show();
                            $(this).find('td').eq(qS).hide();
                            $(this).find('td').eq(qE).hide();
                            $(this).find('td').eq(hS).hide();
                            $(this).find('td').eq(hE).hide();
                            $("#hspGraphicTextSwitch").text("Show Query/Hit Numbers");
                        } else {
                            $(this).find('td').eq(qG).hide();
                            $(this).find('td').eq(hG).hide();
                            $(this).find('td').eq(qS).show();
                            $(this).find('td').eq(qE).show();
                            $(this).find('td').eq(hS).show();
                            $(this).find('td').eq(hE).show();
                            $("#hspGraphicTextSwitch").text("Show Query/Hit Graphics");
                        }
                    }
                );
            });

//TODO: detect if user has uploaded file:
// $(function() {
//    $("input:file").change(function (){
//      var fileName = $(this).val();
//      $(".filename").html(fileName);
//    });
//  });

// download job results
        $("#dlResults").live("click", function(event) {
            //Commenting out old code
            //var string = 'id=' + $(this).attr('data-jobid');
            //$.download(Drupal.settings.xgrid.xgridPath + '/downloadResults.php', string);
            var id = $(this).attr('data-jobid');
            var string = 'download=True';
            $.download('/tool_helpers/rest/' + id + '/results_raw', string);
        });

// download checked sequences
        $("#downloadSequences").live(
            "click",
            function(event) {
                var i = 0;
                var uNames = new Array();
                var string = '';
                var dataType = $("#hspLevelResults").data('data-type');
                $(".hsps:checked").each(
                    function() {
                        if(dataType !== 'Chromosomes') {
                            var title = $(this).attr('hitName');
                            if(uNames.indexOf(title) < 0) {
                                uNames.push(title);
                                string = string + 'hsp' + i + '=' + $(this).val() + '&';
                                i++;
                            }
                        } else {
                            string = string + 'hsp' + i + '=' + $(this).val() + '&';
                            i++;
                        }
                    }
                );
                string = string + 'jobid=' + $("#jobId").text();
                string = string + '&data-db=' + $("#hspLevelResults").data('data-db');
                string = string + '&data-type=' + dataType;
                string = string + '&truncate=' + (dataType === 'Chromosomes');
                if (i > 0) {
                    $.download(Drupal.settings.blast.blastPath + '/downloadSequences.php', string);
                } else {
                    postError("Download", "No sequences selected");
                }
            }
        );

        function postError(errorTitle, message) {
            errorTitle = (typeof errorTitle	=== "undefined") ? "Error Title" : errorTitle;
            message = (typeof message === "undefined") ? "Error Message" : message;
            $("#blastErrorDialog").dialog({
                autoOpen: false,
                show: "scale",
                hide: "scale",
                position: "center",
                width: 600,
                height: 300,
                maxWidth: 750,
                maxHeight: 450,
                draggable: true,
                modal: true,
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                    }
                },
            });
            $("#blastErrorDialog").dialog('option', 'title', errorTitle);
            $("#blastErrorDialog").html(message);
            $("#blastErrorDialog").dialog('open');
        }
// send checked sequences to clustalw
        function sendToClustal(event) {

            var eventTitle = '';
            var HSP_TO_CLUSTALW_HIT_NUM_LIMIT = 50;
            var HSP_TO_CLUSTALW_SEQ_CODE_LIMIT = 3000;
            switch (this.id) {
                case 'sendToClustal':
                    eventTitle = 'Send to Clustalw';
                    HSP_TO_CLUSTALW_HIT_NUM_LIMIT = 50;
                    HSP_TO_CLUSTALW_SEQ_CODE_LIMIT = 3000;
                    break;
                case 'quickAlign':
                    eventTitle = 'Quick align';
                    HSP_TO_CLUSTALW_HIT_NUM_LIMIT = 10;
                    HSP_TO_CLUSTALW_SEQ_CODE_LIMIT = 2000;
                    break;
                default:
                    eventTitle = 'Undefined event type';
                    HSP_TO_CLUSTALW_HIT_NUM_LIMIT = 10;
                    HSP_TO_CLUSTALW_SEQ_CODE_LIMIT = 1000;
            }

            var i = 0;
            var string = '';
            var hitSizes = {};

            // Adding query option
            if (this.id === 'sendToClustal' &&
                $('#hspControl #passWithQuery').attr('checked')) {
                string = string + 'query=' + jobId + '&';
            }
            // END OF Adding query option

            //var dataType = $('#hspLevelResults').data('data-type');

            var bailEarly = false;
            $(".hsps:checked").each(function() {
                var hspId = $(this).val();
                var hspTitle = $(".hsp[id=" + hspId + "]").attr("title");
                if (!(hspTitle in hitSizes)) {
                    hitSizes[hspTitle] = [];
                }

                string = string + 'hsp' + i + '=' + hspId;
                var hstart = $(this).attr("hstart");
                var hend = $(this).attr("hend");
                hitSizes[hspTitle].push(Math.abs(hend - hstart));

                //if('chromosomes' === dataType.toLowerCase()) {
                string = string + ':' + hstart + '-' + hend + '&';
                //}
                //string=string+'&';
                i++;

                // Break out if user has selected too many hits.
                if (i > HSP_TO_CLUSTALW_HIT_NUM_LIMIT) {
                    postError(eventTitle, "Please only select up to " + HSP_TO_CLUSTALW_HIT_NUM_LIMIT + " HSP hits.");
                    bailEarly = true;
                    return false;
                }
            });
            if (bailEarly) {
                return;
            }
            var hitErrors = '';
            for (var hspTitle in hitSizes) {
                hitSizes[hspTitle].forEach(function(hitSize) {
                    if (hitSize > HSP_TO_CLUSTALW_SEQ_CODE_LIMIT) {
                        hitErrors = hitErrors.concat(hspTitle + " (" + hitSize + "), ");
                    }
                });
            }
            hitErrors = hitErrors.substring(0, hitErrors.length - 2);
            if (hitErrors) {
                postError(eventTitle, "You have selected one or more hsp hits with bounded sequence lengths that exceed " + HSP_TO_CLUSTALW_SEQ_CODE_LIMIT + ". If you would like to have the entire (hsp bounded) sequences, please use the \"Download\" button to get them in fasta file format. They are " + hitErrors);
                return;
            }

            if (i == 0) {
                postError(eventTitle, "No sequences selected");
                return;
            }

            // Everything is good. Now do work!
            switch (this.id) {
                case 'sendToClustal':
                    $.customPost('/clustalw', string);
                    break;
                case 'quickAlign':
                    if (i === 1) {
                        postError(eventTitle, "ClustalW needs at least 2 sequences to align.");
                        return;
                    }

                    // conifg the dialog popups
                    $("#hspDialog").dialog({
                        autoOpen: false,
                        show: "scale",
                        hide: "scale",
                        position: "center",
                        width: 750,
                        height: 600,
                        maxWidth: 800,
                        maxHeight: 1200,
                        draggable: true,
                        modal: true,
                    });

                    $("#hspDialog").dialog('open');
                    $("#hspDialog").html('Aligning sequences with ClustalW<br/><img src="' + Drupal.settings.blast.blastPath + '/ajax-loader.gif">');
                    $("#hspDialog").dialog('option', 'title', eventTitle);

                    $.ajax({
                        type: "POST",
                        data: string,
                        url: Drupal.settings.clustalw.clustalwPath + '/quickAlign.php',
                        success: function(msg) {
                            $("#hspDialog").html(msg);
                            $("#hspDialog").dialog('open');
                        }
                    });
                    break;
            }
        }

        $("#sendToClustal").live("click", sendToClustal);
        $("#quickAlign").live("click", sendToClustal);
// send checked sequences to clustalw
        /*$("#quickAlign").live("click",function (event) {
         var i=0;
         var string='';
         var titles = {};
         $(".hsps:checked").each(function() {
         var hspId = $(this).val();
         var hspTitle = $(".hsp[id="+hspId+"]").attr("title");
         if(!(hspTitle in titles)) {
         titles[hspTitle] = 0;
         }
         titles[hspTitle]+=1;
         string=string+'hsp'+i+'='+hspId+'&';
         i++;
         });
         var hspList = '';
         for(var prop in titles){
         if(titles[prop]>1) {
         hspList = hspList.concat(prop + ", ");
         }
         }
         hspList = hspList.substring(0, hspList.length - 2);
         if(hspList) {
         postError("Quick align", "You have selected two or more hits with the same sequence(s) ("+hspList+"). Please select only one of each.");
         return;
         }
         if(i>1){
         // conifg the dialog popups
         $("#hspDialog").dialog({
         autoOpen: false,
         show: "scale",
         hide: "scale",
         position: "center",
         width: 750,
         height: 600,
         maxWidth: 800,
         maxHeight: 1200,
         draggable: true,
         modal: true,
         });

         $("#hspDialog").dialog('open');
         $("#hspDialog").html('Aligning sequences with ClustalW<br/><img src="'+Drupal.settings.blast.blastPath+'/ajax-loader.gif">');
         $("#hspDialog").dialog('option','title',"Quick Align");

         $.ajax({
         type: "POST",
         data: string,
         url: Drupal.settings.clustalw.clustalwPath+'/quickAlign.php',
         success: function(msg){
         $("#hspDialog").html(msg);
         $("#hspDialog").dialog('open');
         }
         });
         } else{
         postError("Quick align", "ClustalW needs at least 2 sequences to align.");
         }
         });*/

// fire the program changed event on page load
        $("input[name='program']", '#blast-ajax-form').trigger('change');

// handle a get/post organism= variable on page load
//        $(".dbs[data-org=" + $(".form-checkbox:checked").parent().parent().attr('data-org') + "]:checkbox").attr('checked', 'checked');
        setAvailableDbs($("input[name='program']:checked", '#blast-ajax-form').val());
        $(".organismCheckboxDiv[data-org=" + $(".form-checkbox:checked").parent().parent().attr('data-org') + "]").trigger('mouseover');

        jQuery.customPost = function(url, data) {
            //url and data options required
            if (url && data) {
                //data can be string of parameters or array/object
                data = typeof data == 'string' ? data : jQuery.param(data);
                //split params into form inputs
                var inputs = '';
                jQuery.each(data.split('&'), function() {
                    var pair = this.split('=');
                    inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
                });
                //send request
                jQuery('<form action="' + url + '" method="post" target="_blank">' + inputs + '</form>')
                    .appendTo('body').submit().remove();
            }
            ;
        };


        jQuery.download = function(url, data, method) {
            //url and data options required
            if (url && data) {
                //data can be string of parameters or array/object
                data = typeof data == 'string' ? data : jQuery.param(data);
                //split params into form inputs
                var inputs = '';
                jQuery.each(data.split('&'), function() {
                    var pair = this.split('=');
                    inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
                });
                //send request
                jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
                    .appendTo('body').submit().remove();
            }
            ;
        };


// add extension to tablesorter pluggin so we can sort scientific notation
        $.tablesorter.addParser({
            // set a unique id
            id: 'scinot',
            is: function(s) {
                return /[+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?/.test(s);
            },
            format: function(s) {
                return $.tablesorter.formatFloat(s);
            },
            type: 'numeric'
        });


        $.tablesorter.addParser({
            // set a unique id
            id: 'links',
            is: function(s) {
// return false so this parser is not auto detected 
                return false;
            },
            format: function(s) {
// format your data for normalization 
                return s.replace(new RegExp(/<.*?>/), "");
            },
// set type, either numeric or text
            type: 'text'
        });


    });
})(jQuery);


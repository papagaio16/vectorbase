<?php

use VectorBase\ToolHelpers\JobSpec;
//require_once(drupal_get_path('module', 'tool_helpers'). '/Lackey.php');
require_once('/vectorbase/web/root/sites/all/modules/custom/tool_helpers/Lackey.php');
class BlastLackey extends VectorBase\ToolHelpers\Lackey {
    protected function buildSubmitFiles(JobSpec $job_specification, $prefix){
	
	if (user_is_logged_in()) {
		$path_to_result_files = DRUPAL_ROOT . "/data/job_results/users";
	}	       
	else {
		$path_to_result_files = DRUPAL_ROOT . "/data/job_results/anonymous";
	}
 
        $path_to_submit_files = DRUPAL_ROOT . "/data/job_submit";
	#$path_to_result_files = DRUPAL_ROOT . "/data/job_results";
        # write single query file
        file_put_contents("$path_to_result_files/$prefix.query.fa",$job_specification->get_input());
        
        # create individual submit files
	$submitFile_str = "#!/bin/sh\n\n" . 
			"source ~condor/software/condor.sh\n\n";
        
        $job_attribs = $job_specification->get_attributes();
        $target_dbs = $job_specification->get_target_dbs();
        
        foreach ($target_dbs as $db) {
            $submit_str =   "universe        = vanilla\n" .
                            "executable      = /opt/local/blast/bin/" . $job_specification->get_program() . "\n" .
                            "output          = $path_to_result_files/$prefix.results.$db.out\n" .
                            "log             = $path_to_result_files/$prefix.results.$db.log\n" .
                            "error           = $path_to_result_files/$prefix.results.$db.err\n" .
                            "\n" .
                            "Arguments = -query $path_to_result_files/$prefix.query.fa -db /vectorbase/dbs/$db ";
            
            foreach ($job_attribs as $attrib => $value) {
                if($attrib != 'server_ip') {
			$submit_str .= "$attrib $value ";
            	}
	    }

	    $submit_str .=  "\n\n" .
                            "Initialdir          = $path_to_result_files\n" .
                            "Transfer_executable = false\n" .
                            "Queue\n";
            
            $submit_filename = "$path_to_submit_files/$prefix" . "_$db.condor";
            file_put_contents($submit_filename,$submit_str);
            
            # add to the dag file string
            $submitFile_str .= "condor_submit $submit_filename\n";
        }
	#Create a condor submission script that will execute blastParse.pl to put results in DB	
	$submit_str = "universe		= vanilla\n" .
		      "executable	= /vectorbase/scripts/condorBlastParse.sh\n" .
		      "Output		= $prefix.parse.out\n" .
		      "Error		= $prefix.parse.err\n" .
		      "Log		= $prefix.parse.log\n" .
		      "\n" .
		      "Arguments	= $prefix " . $job_attribs['-num_alignments'] . " $path_to_result_files " .
	 	      "$prefix.results.*.out $prefix.results.*.err " . count($target_dbs) . " " .  $job_attribs['server_ip'] . "\n\n" .
		      "Initialdir 	= $path_to_result_files\n" .
		      "Transfer_executable = false\n" .
		      "Queue\n";

	$submit_filename = "$path_to_submit_files/$prefix.parse.condor";
	file_put_contents($submit_filename, $submit_str);

        $submitFile_str .= "condor_submit $submit_filename\n";
	#Finally write out to the submit file
	$submitFile_path = "$path_to_submit_files/$prefix.submit";
	file_put_contents($submitFile_path, $submitFile_str);
        
        return array("bash", $submitFile_path);
    }


}


<?php

require_once('Timer.php');

// these are used for viewing chromosome arm/supercontig segments in the genome browser
function blast_generateGFF($id, $br_id, $queryName, $databaseName){
	//For some reason, $databaseName for some of the datasets does not end with gz which
	//causes in the bug whenever the headers in the fasta file are reversed
	//e.g the code that checks if the headers are reversed does not work correclty
        //so the gff files are not created correctly.  So adding replacement here if gz
	//is not found at the end

	if (substr($databaseName, -2) != 'gz') {
		$databaseName .= '.gz';
	}	

	$isTimerEnabled = false; // Turn on/off blast query timers and log

	$hspsQuery = '';
	$bh_ids = null;
	
	if($isTimerEnabled) {	
		$queryTimer = new Timer();
		$writeTimer = new Timer();
		$reverseHeaderTimer = new Timer();
		$allResultsTimer = new Timer();
		$hitsTimer = new Timer();
		$hspsQueryFetchTimer = new Timer();
	}

	// get the tid for "Reversed Headers"
	if($isTimerEnabled) { $reverseHeaderTimer->start(); }
	$rhTidQuery=db_select('taxonomy_term_Data', 't');
	$rhTidQuery->addField('t', 'tid');
	$rhTidQuery->condition('t.name', 'Reversed Headers', '=');
	$rhTid = $rhTidQuery->execute()->fetchField();

	if($isTimerEnabled) { $reverseHeaderTimer->stop(); }

	// look up all blast_results that have this search id

	//$reverseHeadersDbs is an important value that is used taken from previous code the generateGFF.php	
	$reverseHeaderDbs = null;

	$bh_ids=array();
	$hitNames=array();

	$gff='track name="BLAST Hits" description="BLAST Hits" color=green useScore=0 url=http://www.vectorbase.org/tool_helpers/rest/'.$id."/results_raw\n";

	// look up all blast hits for this blast result
	if($isTimerEnabled) { $queryTimer->start(); }
	$blastHitsQuery=db_select('blast_hits', 'bh');
	$blastHitsQuery->addField('bh', 'bh_id');
	$blastHitsQuery->addField('bh', 'name');	
	$blastHitsQuery->addField('bh', 'description');
	$blastHitsQuery->condition('bh.br_id', $br_id, '=');
	$hits = $blastHitsQuery->execute();
	if($isTimerEnabled) { $queryTimer->stop(); }
	while(($hit = $hits->fetchAssoc()) !== false){
		$bh_ids[]=$hit['bh_id'];
		$hitNames[$hit['bh_id']]=$hit['name'];
		//$hitDescriptions[]=$hit->description;
	}

	// get hsp info for these hits
	if($bh_ids){ // check for results
		foreach($hitNames as  $key => $value){

			// change some of the hit names so the gffs show up in ensembl properly
			// these should all be tagged with "Reversed Headers"
	
			// get the nid(entity_id) for this db name
			if(is_null($reverseHeaderDbs)) {
				if($isTimerEnabled) { $queryTimer->start(); }
				$isTaggedQuery = db_select('file_managed', 'fm');
				$isTaggedQuery->addField('fm', 'fid');
				$isTaggedQuery->join('field_data_field_file', 'ff', 'fm.fid = ff.field_file_fid');
				$isTaggedQuery->join('field_data_field_tags', 'ft', 'ft.entity_id = ff.entity_id');
				$isTaggedQuery->condition('fm.filename', $databaseName, '=');
				$isTaggedQuery->condition('ft.field_tags_tid', $rhTid, '=');
				$isTaggedQuery = $isTaggedQuery->countQuery();
				$isTagged = $isTaggedQuery->execute()->fetchAssoc();
				
				//Adding this check instead of the commented out below to see if the fasta
				//file has a reverse header because it seems this is what is returned
				//if there are results from the database
				$reverseHeaderDbs = $isTagged['expression'];
				//$reverseHeaderDbs = ($isTagged != false && !empty($isTagged['count']));
				if($isTimerEnabled) { $queryTimer->stop(); }
			}
			if($reverseHeaderDbs){
				preg_match("#^.*?:.*?:(.*?):#",$value,$match);
				$hitNames[$key]=$match[1];
			}
		}

		if($isTimerEnabled) { $hitsTimer->start(); }			
		$hspsQuery = db_select('blast_hsps', 'bh');
		$hspsQuery->addField('bh', 'bh_id');
		$hspsQuery->addField('bh', 'starthit');
		$hspsQuery->addField('bh', 'endhit');
		$hspsQuery->addField('bh', 'score');
		$hspsQuery->addField('bh', 'strandhit');
		$hspsQuery->condition('bh.bh_id', $bh_ids, 'IN');
		$hspsQuery->orderBy('bh.bh_id');
		$hsps = $hspsQuery->execute();
		if($isTimerEnabled) { $hitsTimer->stop(); }

		$hsp = $hsps->fetchAssoc();
		if($hsp !== false) {
			$lastDistinctBlastId = $hsp['bh_id'];
			do {
				if($lastDistinctBlastId !== $hsp['bh_id']) {
					$lastDistinctBlastId = $hsp['bh_id'];
				}
				if($hsp['strandhit']=='1')
					$strand="+";
				else if($hsp['strandhit']=='-1')
					$strand="-";
				else 
					$strand=".";
	
				$gff.=$hitNames[$lastDistinctBlastId]."\tBLAST\tBlast Hit\t".$hsp['starthit']."\t".$hsp['endhit']."\t".$hsp['score']."\t".$strand."\t.\t".$queryName."\n";
			//      hit name        BLAST   Blast Hit   start   end     length  strand  .   query name
			} while(($hsp = $hsps->fetchAssoc()) !== false); 
		}
				
		$location=$_SERVER['DOCUMENT_ROOT']."/data/";
		$fileName=$id."_".$br_id.".gff";
		if($isTimerEnabled) { $writeTimer->start(); }
		file_put_contents($location.$fileName,$gff);
		if($isTimerEnabled) { $writeTimer->stop(); }
	} // end of creating a GFF for a blast result

	if($isTimerEnabled) { 
		$timingData = "\n" . date('H:i:s, d M Y') . " Timing data for gff file generation for job $id / $searchId\n";
		$timingData .= "\tReverse header query took: " . $reverseHeaderTimer->toString() . " seconds\n";
		$timingData .= "\tAll blast results query took: " . $allResultsTimer->toString() . " seconds\n";
		$timingData .= "\tIs tagged queries (3n) took: " . $queryTimer->toString() . " seconds\n";
		$timingData .= "\tHits query took: " . $hitsTimer->toString() . " seconds\n";
		$timingData .= "\tFile writing took: " . $writeTimer->toString() . " seconds\n";	
		//$timingData .= "\tBlast hit ids (should match the number of ?'s in the hists query) (size: " . count($bh_ids) . '):  ' . print_r($bh_ids, true) . "\n";
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/data/blastTimingDataForGffFileGeneration.log", $timingData, FILE_APPEND); 
	}

}

<?php


/**
 * @file
 * Install, update and uninstall functions for the VectorBase Search module.
 */

/**
 * Implements hook_schema().
 */

function vbsearch_schema() {
  $schema = array();

  $schema['vbsearch'] = array(
    'description' => 'The base table for VectorBase Search.',
    'fields' => array(
      'vbsid' => array(
        'description' => 'The primary identifier for VectorBase Search.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type (bundle) of this VectorBase Search.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => 'The title given to the VectorBase Search.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'ID of Drupal user who did the VectorBase search.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the VectorBase search was done.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the VectorBase search was most recently done.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'description' => array(
        'description' => 'VectorBase Search description.',
        'type' => 'text',
      ),
    ),
    'primary key' => array('vbsid'),
  );

  $schema['vbsearch_type'] = array(
    'description' => 'Stores information about all defined VectorBase Search types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique VectorBase Search type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}

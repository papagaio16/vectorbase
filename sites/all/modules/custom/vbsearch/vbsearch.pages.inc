<?php

/**
* VectorBase Search view callback.
*/
function vbsearch_view($task) {
  drupal_set_title(entity_label('vbsearch', $task));
  return entity_view('vbsearch', array(entity_id('vbsearch', $task) => $task), 'full');
}

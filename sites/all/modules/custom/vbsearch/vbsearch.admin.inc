<?php

/**
 * Creates the VectorBase Search Administration page.
 * It lists the items that can be configured.
 *
 * @return
 *   The output HTML
 */
function vbsearch_admin_menu_page() {
  $item = menu_get_item();
  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('You do not have any VectorBase Search administrative items.');
  }

  return $output;
}

function solr_fields_edit_form($form, &$form_state, $solr_field = array()) {

  if(empty($solr_field)) {
    unset($solr_field);
  }

  $form['field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Solr Field Name'),
    '#default_value' => isset($solr_field) ? $solr_field['field_name'] : '',
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => isset($solr_field) ? $solr_field['description'] : '',
    '#required' => FALSE,
  );

  $form['solr_parameters'] = array(
    '#type' => 'textfield',
    '#title' => t('Solr Parameters'),
    '#default_value' => isset($solr_field) ? $solr_field['solr_parameters'] : '',
    '#required' => TRUE,
    '#description' => t('Specify the Solr parameters this field will be used in separated by a comma e.g fl, fq, qf etc.'),
  );

  $form['boost'] = array(
    '#type' => 'textfield',
    '#title' => t('Query Field Boost'),
    '#default_value' => isset($solr_field) ? $solr_field['boost'] : '',
    '#required' => FALSE,
    '#description' => t('Specify the importance a field will have in the search.  Currently it uses a query field boost only.'),
  );

  $form['ranged_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ranged Field'),
    '#required' => FALSE,
    '#description' => t('Specify whether this Solr field will be used to search for ranges e.g dates'),
  );

  $form['indexing_mapping_field'] = array(
    '#type' => 'textarea',
    '#title' => t('Content Types Field Mapping'),
    '#default_value' => isset($solr_field) ? $solr_field['indexing_mapping_field'] : '',
    '#required' => FALSE,
    '#description' => t('Specify what fields of a content type should be mapped to this solr field e.g Newsletters|field_name'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['save'] = array(
    '#type' => 'submit',
    //Validate function will be added in the future
    //'#validate' => array('solr_field_edit_validate'),
    '#submit' => array('solr_field_edit_form_submit'),
    '#value' => t('Save'),
  );

  if (!empty($solr_field)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#submit' => array('apachesolr_environment_edit_delete_submit'),
      '#value' => t('Delete'),
    );
  }

  // Ensures destination is an internal URL, builds "cancel" link.
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $destination = $_GET['destination'];
  }
  else {
    $destination = 'admin/config/search/vbsearch/solr_fields';
  }
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $destination,
  );

  return $form;
}

function solr_field_edit_form_submit($form, &$form_state) {
  $data = file('/vectorbase/web/root/data/configuration/solr_search_fields.txt'); // reads an array of lines
  $data_tmp = $data;
  $replaced = false;
  $config_line = $form_state['values']['field_name'] . ':' . $form_state['values']['description'] . ':' . $form_state['values']['solr_parameters'] . ':' .
    $form_state['values']['boost'] . ':' . $form_state['values']['ranged_field'] . ':' . preg_replace('/(\r\n|\r|\n)/s', "$", $form_state['values']['indexing_mapping_field']) . "\n";
  foreach ($data as $key => $value) {
    if (stristr($value, $form_state['values']['field_name'])) {
      $data_tmp[$key] = $config_line;
      $replaced = true;
    }
  }

  if (!$replaced){
    $data_tmp[] =  $config_line;
  }

  file_put_contents('/vectorbase/web/root/data/configuration/solr_search_fields.txt', implode('', $data_tmp));

  $form_state['redirect'] = 'admin/config/search/vbsearch/solr_fields';
}

function vbsearch_solr_searchable_fields(array $form, array $form_state) {

  $data = file('/vectorbase/web/root/data/configuration/solr_search_fields.txt');
  $headers = '';

  $rows = array();

  $headers = array(
    array('data' => t('Solr Field Name'), 'colspan' => 2),
    array('data' => t('Description'), 'colspan' => 1),
    array('data' => t('Parameters'), 'colspan' => 1),
    array('data' => t('Boost'), 'colspan' => 1),
    array('data' => t('Ranged'), 'colspan' => 1),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  foreach ($data as $values) {
    $ops = array();
    $solr_fields_values = explode(':', $values);

    $ops['edit'] = array(
      'class' => 'operation',
      'data' => l(t('Edit'),
        'admin/config/search/vbsearch/solr_fields/' . $solr_fields_values[0] . '/edit',
        array('query' => array('destination' => current_path()))
      ),
    );

    $ops['delete'] = array(
      'class' => 'operation',
      'data' => l(t('Delete'),
        'admin/config/search/vbsearch/solr_fields/' . $solr_fields_values[0] . '/delete',
        array('query' => array('destination' => $_GET['q']))
      ),
    );

    $solr_field_name = l($solr_fields_values[0],
      'admin/config/search/vbsearch/solr_fields/' . $solr_fields_values[0] . '/edit',
      array('query' => array('destination' => $_GET['q']))
    );

    $solr_fields_class_row = '';
    $class = '';

    $rows[$solr_fields_values[0]] = array('data' =>
      array(
        // Cells
        array(
          'class' => 'status-icon',
          'data' => '<div title="' . $class . '"><span class="element-invisible">' . $class . '</span></div>',
        ),
        array(
          'class' => $solr_fields_class_row,
          'data' => $solr_field_name,
        ),
      ),
      'class' => array(drupal_html_class($class)),
    );
    // Add the links to the page

    $description = array(
      array(
        'data' => $solr_fields_values[1] == '' ? t('<i>No Description Available</i>') : $solr_fields_values[1],
      ),
    );

    $parameters = array(
      array(
        'data' => $solr_fields_values[2],
      ),
    );

    $boost = array(
      array(
        'data' => $solr_fields_values[3] == '' ? t('<i>Default</i>') : $solr_fields_values[3],
      ),
    );

    $ranged_field = array(
      array(
        'data' => $solr_fields_values[4] != 0 ? t('Ranged') : t('Not Ranged'),
      ),
    );

    $rows[$solr_fields_values[0]]['data'] = array_merge($rows[$solr_fields_values[0]]['data'], $description);
    $rows[$solr_fields_values[0]]['data'] = array_merge($rows[$solr_fields_values[0]]['data'], $parameters);
    $rows[$solr_fields_values[0]]['data'] = array_merge($rows[$solr_fields_values[0]]['data'], $boost);
    $rows[$solr_fields_values[0]]['data'] = array_merge($rows[$solr_fields_values[0]]['data'], $ranged_field);
    $rows[$solr_fields_values[0]]['data'] = array_merge($rows[$solr_fields_values[0]]['data'], $ops);
  }

  $form['vbsearch_solr_fields_settings']['actions'] = array(
    '#markup' => '<ul class="action-links">' . drupal_render($actions) . '</ul>',
  );

  if(!empty($rows)) {
    $form['vbsearch_solr_fields_settings']['table'] = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => array_values($rows),
      '#attributes' => array('class' => array('admin-apachesolr')),
    );
  }

  //$vbsearch_name = l($data)
  //return "Here you will be able to see the different search types and manage them";

  return $form;
}

function vbsearch_type_field_config_form($form, &$form_state, $vbsearch_type, $vbsearch_type_field) {

  $form['field_name'] = array(
    '#type' => 'value',
    '#value' => $vbsearch_type_field['field_name'],
  );

  $form['vbsearch_type'] = array(
    '#type' => 'value',
    '#value' => $vbsearch_type->type,
  );

  $form['solr_mapping'] = array(
    '#title' => t('Solr Fields Mapping'),
    '#type' => 'textarea',
    '#default_value' => isset($vbsearch_type_field) ? $vbsearch_type_field['solr_mapping'] : '',
    '#description' => t('The solr fields to map to.  When searching, the fields listed will be used to search against.'),
  );

  $form['visible'] = array(
    '#title' => t('Visibility Behavior'),
    '#type' => 'textarea',
    '#default_value' => isset($vbsearch_type_field) ? $vbsearch_type_field['visible'] : '',
    '#description' => t('The value that needs to be selected in order for it to be visible.')
  );

  $form['placeholder'] = array(
    '#title' => t('Plaeholder'),
    '#type' => 'textfield',
    '#default_value' => isset($vbsearch_type_field) ? $vbsearch_type_field['placeholder'] : '',
    '#description' => t('The placeholder you would like to give to the field.  Currently is visible for every field type, but will only be available for text fields in the future'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  return $form;
}

function vbsearch_type_field_config_form_submit($form, &$form_state) {
  //Use drupal_write_record to save the values to the database

  //Using this for now
  $data = file('/vectorbase/web/root/data/configuration/configuration.txt'); // reads an array of lines
  $data_tmp = $data;
  $replaced = false;
  $config_line = $form_state['values']['field_name'] . ',' . $form_state['values']['vbsearch_type'] . ',' . preg_replace('/(\r\n|\r|\n)/s',":", $form_state['values']['solr_mapping']) . ',' .
    preg_replace('/(\r\n|\r|\n)/s', ":", $form_state['values']['visible']) . ',' . $form_state['values']['placeholder'] . "\n";
  foreach ($data as $key => $value) {
    if (stristr($value, $form_state['values']['field_name'])) {
      $data_tmp[$key] = $config_line;
      $replaced = true;
    }
  }

  if (!$replaced){
    $data_tmp[] =  $config_line;
  }

  file_put_contents('/vectorbase/web/root/data/configuration/configuration.txt', implode('', $data_tmp));

  $form_state['redirect'] = 'admin/structure/vbsearch/manage/advanced_search/fields';
}

/**
 * Generates the VectorBase Search type editing form.
 */
function vbsearch_type_form($form, &$form_state, $vbsearch_type, $op = 'edit') {

  if ($op == 'clone') {
    $vbsearch_type->label .= ' (cloned)';
    $vbsearch_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $vbsearch_type->label,
    '#description' => t('The human-readable name of this vbsearch type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($vbsearch_type->type) ? $vbsearch_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $vbsearch_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'vbsearch_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this vbsearch type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($vbsearch_type->description) ? $vbsearch_type->description : '',
    '#description' => t('Description about the vbsearch type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save vbsearch type'),
    '#weight' => 40,
  );

  if (!$vbsearch_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete vbsearch type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('vbsearch_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing vbsearch_type.
 */
function vbsearch_type_form_submit(&$form, &$form_state) {
  $vbsearch_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  vbsearch_type_save($vbsearch_type);

  // Redirect user back to list of vbsearch types.
  $form_state['redirect'] = 'admin/structure/vbsearch';
}

function vbsearch_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/vbsearch/' . $form_state['vbsearch_type']->type . '/delete';
}

/**
 * VectorBase Search type delete form.
 */
function vbsearch_type_form_delete_confirm($form, &$form_state, $vbsearch_type) {
  $form_state['vbsearch_type'] = $vbsearch_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['vbsearch_type_id'] = array('#type' => 'value', '#value' => entity_id('vbsearch_type' ,$vbsearch_type));
  return confirm_form($form,
    t('Are you sure you want to delete vbsearch type %title?', array('%title' => entity_label('vbsearch_type', $vbsearch_type))),
    'vbsearch/' . entity_id('vbsearch_type' ,$vbsearch_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * VectorBase Search type delete form submit handler.
 */
function vbsearch_type_form_delete_confirm_submit($form, &$form_state) {
  $vbsearch_type = $form_state['vbsearch_type'];
  vbsearch_type_delete($vbsearch_type);

  watchdog('vbsearch_type', '@type: deleted %title.', array('@type' => $vbsearch_type->type, '%title' => $vbsearch_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $vbsearch_type->type, '%title' => $vbsearch_type->label)));

  $form_state['redirect'] = 'admin/structure/vbsearch';
}


/**
 * Load Bulk Operations page.
 */
function vbsearch_bulk_ops_page() {
    header("Location: /vbsearch/bulkops");
}



/**
 * Page to select vbsearch Type to add new vbsearch.
 */
function vbsearch_admin_add_page() {
  $items = array();
  foreach (vbsearch_types() as $vbsearch_type_key => $vbsearch_type) {
    $items[] = l(entity_label('vbsearch_type', $vbsearch_type), 'vbsearch/add/' . $vbsearch_type_key);
  }
  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of vbsearch to create.')));
}

/**
 * Add new vbsearch page callback.
 */
function vbsearch_add($type) {
  $vbsearch_type = vbsearch_types($type);

  $vbsearch = entity_create('vbsearch', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('vbsearch_type', $vbsearch_type))));

  $output = drupal_get_form('vbsearch_form', $vbsearch);

  return $output;
}

/**
 * VectorBase Search Form.
 */
function vbsearch_form($form, &$form_state, $vbsearch) {
  $form_state['vbsearch'] = $vbsearch;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $vbsearch->title,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $vbearch->description,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $vbsearch->uid,
  );

  field_attach_form('vbsearch', $vbsearch, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save vbsearch'),
    '#submit' => $submit + array('vbsearch_form_submit'),
  );

  // Show Delete button if we edit vbsearch.
  $vbsearch_id = entity_id('vbsearch' ,$vbsearch);
  if (!empty($vbsearch_id) && vbsearch_access('edit', $vbsearch)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('vbsearch_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'vbsearch_form_validate';

  return $form;
}

function vbsearch_form_validate($form, &$form_state) {

}


/**
 * VectorBase Search submit handler.
 */
function vbsearch_form_submit($form, &$form_state) {
  $vbsearch = $form_state['vbsearch'];

  entity_form_submit_build_entity('vbsearch', $vbsearch, $form, $form_state);

  vbsearch_save($vbsearch);

  $vbsearch_uri = entity_uri('vbsearch', $vbsearch);

  $form_state['redirect'] = $vbsearch_uri['path'];

  drupal_set_message(t('VectorBase Search %title saved.', array('%title' => entity_label('vbsearch', $vbsearch))));
}


function vbsearch_form_submit_delete($form, &$form_state) {
  $vbsearch = $form_state['vbsearch'];
  $vbsearch_uri = entity_uri('vbsearch', $vbsearch);
  $form_state['redirect'] = $vbsearch_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function vbsearch_delete_form($form, &$form_state, $vbsearch) {
  $form_state['vbsearch'] = $vbsearch;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['vbsearch_type_id'] = array('#type' => 'value', '#value' => entity_id('vbsearch' ,$vbsearch));
  $vbsearch_uri = entity_uri('vbsearch', $vbsearch);
  return confirm_form($form,
    t('Are you sure you want to delete vbsearch %title?', array('%title' => entity_label('vbsearch', $vbsearch))),
    $vbsearch_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function vbsearch_delete_form_submit($form, &$form_state) {
  $vbsearch = $form_state['vbsearch'];
  vbsearch_delete($vbsearch);

  drupal_set_message(t('VectorBase Search %title deleted.', array('%title' => entity_label('vbsearch', $vbsearch))));

  $form_state['redirect'] = '/vbsearch/';
}

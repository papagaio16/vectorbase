// Javascript for the timeout error page
//
// Hides unwanted elements on the page
//

function HideElements(){
	document.getElementById("block-superfish-1").style.visibility = "hidden";
	document.getElementById("breadcrumbs").style.visibility = "hidden";
	document.getElementById("edit-submit--2").style.visibility = "hidden";
	document.getElementById("block-search-form").style.visibility = "hidden";
}


function Check_Load(){
        // Delays setting the status of the download button until page load is complete
        // So that the total number of records returned can be accurately checked.
        if(document.getElementById("block-superfish-1")){
//		alert('Load complete');
                clearInterval(ThisTimer);
                HideElements();
        }
}

document.addEventListener("onload", ThisTimer = setInterval("Check_Load()",100));

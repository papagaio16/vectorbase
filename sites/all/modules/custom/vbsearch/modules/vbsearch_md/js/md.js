var ThisTimer;
function Set_md_Status(){
	var results_count = document.getElementById('total_items').innerHTML.trim();
	var thisElement = document.getElementById('myBtn');
	if(results_count <= 30000){
		//Enable the button/link
		thisElement.disabled = false;
	}else{
		//Disable the button/link
		thisElement.disabled = true;
		thisElement.style.background = "linear-gradient(to bottom, #D8D8D8 0%,#A4A4A4 100%)";
	}
}

function Check_Load(){
	// Delays setting the status of the download button until page load is complete
	// So that the total number of records returned can be accurately checked.
	if(document.getElementById("total_items")){
		clearInterval(ThisTimer);
		Set_md_Status();
	}
}

function Get_Download(){
	// Call the Drupal page
	window.open('/vbsearch/download');
}

function Load_Timer(){
        setInterval("Check_Load()",100)
}

document.addEventListener("onload", Load_Timer());


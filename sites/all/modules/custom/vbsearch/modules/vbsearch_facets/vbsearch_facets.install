<?php

/**
 * Implements hook_schema
 *
 * It will create the tables that will be used by the VectorBase Search Facet
 * module to dynamically create facets for pages
 **/

function vbsearch_facets_schema() {
  $schema['vbsearch_facet'] = array(
    'description' => t('The base table for the VectorBase Search facets.'),
    'fields' => array(
      'vbsearch_facet_id' => array(
        'description' => t('The primary identifier for a VectorBase Search facet.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'label' => array(
        'description' => t('Human readable name for facet used to name the block'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default'  => '',
      ),
      'description' => array(
        'description' => t('The description of the VectorBase Search facet.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => t('The title of the VectorBase Search facet.'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'delta' => array(
        'description' => t('The machine name for the VectorBase Search facet.'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'vbsearch_facet_delta' => array('delta'),
    ),
    'unique keys' => array(
      'delta' => array('delta'),
    ),
    'primary key' => array('vbsearch_facet_id'),
  );

  $schema['vbsearch_facet_category'] = array(
    'description' => t('Defines the categories that could be used for facets created'),
    'fields' => array(
      'facet_category_id' => array(
        'description' => t('Primary identifier for a VectorBase Search Facet Category.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'description' => array(
        'description' => t('Used to describe the facet category'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'delta' => array(
        'description' => t('Machine readable name for a VectorBase Search facet category'),
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'multiselect' => array(
        'description' => t('Flag to know if VectorBase Search facet category should be multiselect'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'sortable' => array(
        'description' => t('Flag to know if VectorBase Search facet category should be sortable'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'relationship' => array(
        'description' => t('List of other categories needed to be selected for category to appear'),
        'type' => 'text',
        'serialize' => TRUE,
      ),
    ),
    'indexes' => array(
      'vbsearch_facet_category_delta' => array('delta'),
    ),
    'unique keys' => array(
      'delta' => array('delta'),
    ),
    'primary key' => array('facet_category_id'),
  );

  $schema['vbsearch_facet_categories'] = array(
    'description' => t('Maps which categories belong to which facet that was created'),
    'fields' => array(
      'vbsearch_facet_id' => array(
        'description' => t('Foreign key used to map the category to a facet.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'facet_category_id' => array(
        'description' => t('Foreign key to map a facet to a category.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'default_category' => array(
        'description' => t('Flag to VectorBase Search Facet category displayed'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'label' => array(
        'description' => t('Used as Display Name in the VectorBase Search facet.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'description' => t('Used to order the category in the VectorBase Search facet'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('vbsearch_facet_id', 'facet_category_id'),
    'foreign keys' => array(
      'affected_vbsearch_facet' => array(
        'table' => 'vbsearch_facet',
        'columns' => array('vbsearch_facet_id' => 'vbsearch_facet_id'),
      ),
      'affected_vbsearch_facet_category' => array(
        'table' => 'vbsearch_facet_category',
        'columns' => array('facet_category_id' => 'facet_category_id'),
      ),
    ),
    'indexes' => array(
      'vbsearch_facet_categories' => array('vbsearch_facet_id'),
      'vbsearch_category_facets' => array('facet_category_id'),
    ),
  );

  return $schema;
}


/**
 * Implements hook_install()
 *
 * Used to add the foreign constraint to the vbsearch_facet_categories table
 * since the Schema API's foreign key syntax is only for documentation
 * purposes (it doesn't really add the foreign key constraint).
 *
 * It also adds the default facet that is used by the Search Page to the
 * database.
 **/

function vbsearch_facets_install() {
  //Make foreign keys.
  db_query('
    ALTER TABLE {vbsearch_facet_categories}
    ADD CONSTRAINT {affected_vbsearch_facet}
    FOREIGN KEY (vbsearch_facet_id) REFERENCES {vbsearch_facet} (vbsearch_facet_id)
  ');

  db_query('
    ALTER TABLE {vbsearch_facet_categories}
    ADD CONSTRAINT {affected_vbsearch_facet_category}
    FOREIGN KEY (facet_category_id) REFERENCES {vbsearch_facet_category} (facet_category_id)
  ');
}


/**
 * Implements hook_uninstall()
 *
 * Undo what was done during the hook_install() implementation of the module.
 **/

function vbsearch_facets_uninstall() {
  //Drop real foreign keys
  db_query('
    ALTER TABLE {vbsearch_facet_categories}
    DROP CONSTRAINT {affected_vbsearch_facet}
  ');

  db_query('
    ALTER TABLE {vbsearch_facet_categories}
    DROP CONSTRAINT {affected_vbsearch_facet_category}
  ');
}

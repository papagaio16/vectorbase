(function (VectorBaseSearch, $, undefined) {

  if (VectorBaseSearch.data == undefined) {
    VectorBaseSearch.data = {};
  }

  VectorBaseSearch.init_autocomplete = function() {
    jQuery("#block-search-form :text, #search-form :text").autocomplete({
      source: function (request, response) {
        var field_id = this.element.attr('id');
        var field_name = this.element.attr('name').split('[')[0];
        var base_url = '/vbsearch/autocomplete/';
        var url = base_url + request.term;

        if (field_id != 'edit-keys' && field_id != 'edit-search-block-form--2' && field_name != 'q') {
          var url = base_url + request.term + '/' + field_name;
        }

        if(VectorBaseSearch.data.xhr && VectorBaseSearch.data.xhr.readyState != 4) {
          VectorBaseSearch.data.xhr.abort();
        }

        VectorBaseSearch.data.xhr = $.ajax({
          url: url, type: "GET", dataType: "json",
          success: function (data) {
            response(data);
          },
          error: function(xhr, statusText, err) {
            var absoluteURL = 'https://www.vectorbase.org' + this.url;
            VectorBaseSearch.data.xhr = $.ajax({
              url: absoluteURL, type: "GET", dataType: "json",
              success: function (data) {
                response(data);
              }
            })
          }
        })
      }
    });
  };

  $(document).ready(function () {
    VectorBaseSearch.init_autocomplete();
  });
})(window.VectorBaseSearch = window.VectorBaseSearch || {}, jQuery);

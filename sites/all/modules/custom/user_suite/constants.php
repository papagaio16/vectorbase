<?php


/** PUBLIC directory to which all users' job results are saved.
 * Currently, we have no need to secure these results. This
 * is simply a convenience feature which allows a registered
 * user to save results. The user, will, however, be the
 * only one allowed to save/expire those results.
 */
define('USER_JOB_RESULTS_SAVE_DIRECTORY', 'saved_results');

/** URL to the xgrid SOAP-driven interface */
define('XGRID_URL', 'http://jobs.vectorbase.org/xgrid.wsdl');

// Keys to identify entries in the vb_drupal.xgrid_job_params table.
define('USER_JOB_XGRID_PATH_ARGUMENT', 'results_file');
define('USER_JOB_ID', 'job_id');
define('USER_GRID_JOB_ID', 'condor_job_id');
define('USER_JOB_RAW_ID', 'rawId');
define('USER_JOB_PROGRAM', 'program');
define('USER_JOB_XGRID_DESCRIPTION_ARGUMENT', 'description');
define('USER_JOB_VALUE', 'value');
define('USER_JOB_ARGUMENT', 'argument');
define('USER_JOB_DATE', 'date');
define('USER_JOB_TIME', 'time');


// Status messages
define('USER_JOB_PARAMETERS_NOT_FOUND', 'USER_JOB_PARAMETERS_NOT_FOUND');
define('USER_JOB_SAVE_SQL_FAILED', 'USER_JOB_SAVE_SQL_FAILED');
define('USER_JOB_WRONG_STATUS', 'USER_JOB_WRONG_STATUS');
define('USER_JOB_MKDIR_FAILED', 'USER_JOB_MKDIR_FAILED');
define('USER_JOB_SAVE_FAILED', 'USER_JOB_SAVE_FAILED');
define('USER_JOB_DELETE_SQL_FAILED', 'USER_JOB_DELETE_SQL_FAILED');
define('USER_JOB_DELETE_FAILED', 'USER_JOB_DELETE_FAILED');
define('USER_JOB_DESC_IS_SAME', 'USER_JOB_DESC_IS_SAME');
define('USER_JOB_OP_EXE_SUCCESS', 'USER_JOB_OP_EXE_SUCCESS');

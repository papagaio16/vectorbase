
// Bind code taken from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
if (!Function.prototype.scopeBind) {
	Function.prototype.scopeBind = function (oThis) {
		if (typeof this !== "function") {
			// closest thing possible to the ECMAScript 5 internal IsCallable function
			throw new TypeError("Function.prototype.scopeBind - what is trying to be bound is not callable");
		}
		var aArgs = Array.prototype.slice.call(arguments, 1), 
		    fToBind = this, 
		    fNOP = function () {},
		    fBound = function () {
			    return fToBind.apply(this instanceof fNOP && oThis
					    ? this
					    : oThis,
					    aArgs.concat(Array.prototype.slice.call(arguments)));
		    };
		fNOP.prototype = this.prototype;
		fBound.prototype = new fNOP();
		return fBound;
	};
} else {
	console.log('Not overriding Function.prototype.scopeBind');
}


jq1102(document).ready(function() {
	/* Kick off the save/unsave of a job's results.
 	 * Also toggle the star button icon color. */
	jq1102('button.btn[data-toggle]').on(
		'click', 
		function() {
		jq1102(this).find('.icon-star-empty').toggleClass('icon-white').toggleClass('icon-yellow');
		jq1102(this).find('i').toggleClass('icon-spinner icon-spin');			
		this.operation = !jq1102(this).is('.active') ? 'save' : 'delete';
		this.jobRow = jq1102(this).closest('.job-row');
		this.alertWait = 5000;
		jq1102.ajax({
			type: 'POST',
			data: 'jobId=' + this.jobRow.attr('job-id') + '&realId=' + this.jobRow.attr('real-id') + '&op=' + this.operation + '&uid=' + jq1102('#accordion-jobs').attr("uid") + '&sid=' + jq1102('#accordion-jobs').attr("sid"),
			url: '/sites/all/modules/custom/user_suite/job_save.php',
			context: this,
			error: function(response) {
				console.log('Error: ' + response);
				this.jobRow.find('.icon-star-empty').toggleClass('icon-white').toggleClass('icon-yellow').parent().button('toggle');
				this.daButton = this.jobRow.find('#savecol button');
				this.daButton.popover({trigger:'manual', placement:'right', content:'Error: ' + response}).popover('show');
				this.expirePopup = function() {
					this.daButton.popover('destroy');
				}
				setTimeout(this.expirePopup.scopeBind(this), this.alertWait);
				jq1102(this).find('i').toggleClass('icon-spinner icon-spin');
			},
			success: function(response) {
				results = response.split(':');
				daButton = this.jobRow.find('#savecol button');
				if(results[0] == 'USER_JOB_OP_EXE_SUCCESS') {
					console.log('Success: ' + response);
					daLink = this.jobRow.find('#linkcol a');
					if(this.operation == 'save') {
						daLink.attr('href', results[1]);
						daButton.css('color', 'green');
					} else if(this.operation == 'delete') {
						daLink.attr('href', '/data/job_results/users/'+this.jobRow.attr('job-id'));
						daButton.removeAttr('style');
					}
				} else if(results[0] == 'USER_JOB_WRONG_STATUS') {
					console.log('Wrong status: ' + response);
					this.jobRow.find('.icon-star-empty').toggleClass('icon-white').toggleClass('icon-yellow').parent().button('toggle');
					daButton.popover({trigger:'manual', placement:'right', content:response}).popover('show');
					expirePopup = function() {
						daButton.popover('destroy');
					}
					setTimeout(expirePopup.scopeBind(this), this.alertWait);
				} else {
					console.log('Other response: ' + response);
					daButton.popover({trigger:'manual', placement:'right', content:response}).popover('show');
					expirePopup = function() {
						daButton.popover('destroy');
					}
					setTimeout(expirePopup.scopeBind(this), this.alertWait);
				}
				jq1102(this).find('i').toggleClass('icon-spinner icon-spin');
			}
		});
	});
	
	jq1102('.editButton').on(
		'click',
		function() {
			var desc = jq1102(this).closest('.job-row').find('#desccol .description').text();
			var modForm = jq1102('#editModalForm');
			modForm.find('#editDescText').val(desc);
			var jobId = jq1102(this).closest('.job-row').attr('job-id');
			modForm.attr('job-id', jobId);
			var msgDiv = modForm.find('#modal-body-message');
			if(!msgDiv.hasClass('hide')) {
				msgDiv.toggleClass('hide');	
			}
			if(msgDiv.hasClass('alert')) {
				msgDiv.toggleClass('alert');	
			}
			if(msgDiv.hasClass('alert-error')) {
				msgDiv.toggleClass('alert-error');	
			}
			if(msgDiv.hasClass('alert-success')) {
				msgDiv.toggleClass('alert-success');	
			}
			modForm.modal("show");
		}
	);

	jq1102('#editSubmitButton').on(
		'click',
		function(e) {
			e.preventDefault()
			var jobId = jq1102(this).closest('#editModalForm').attr('job-id');
			jq1102(this).button('saving');
			var desc = jq1102(this).closest('#editModalForm').find('#editDescText').val();
			var msgDiv = jq1102(this).closest('#editModalForm').find('#modal-body-message');
			if(!msgDiv.hasClass('hide')) {
				msgDiv.toggleClass('hide');	
			}
			if(msgDiv.hasClass('alert')) {
				msgDiv.toggleClass('alert');	
			}
			if(msgDiv.hasClass('alert-error')) {
				msgDiv.toggleClass('alert-error');	
			}
			if(msgDiv.hasClass('alert-success')) {
				msgDiv.toggleClass('alert-success');	
			}
			jq1102.ajax({
				type: "POST",
				data: 'jobId=' + jobId  + '&op=edit-description&opVal='+ desc +'&uid=' + jq1102('#accordion-jobs').attr("uid") + '&sid=' + jq1102('#accordion-jobs').attr("sid"),
				url: "/sites/all/modules/custom/user_suite/job_save.php",
				context: jq1102('#editModalForm'), // Modal form
				error: function(response) {
					msgDiv.toggleClass('hide alert alert-error').text(response);
					jq1102(this).find('#editSubmitButton').button('reset');
				},
				success: function(response) {
					if(response == 'USER_JOB_OP_EXE_SUCCESS') {
						msgDiv.toggleClass('hide alert alert-success').text('Description edit success. Updating form description to: "'+desc+'"');
						var descDiv = jq1102('.job-row[job-id="'+jobId+'"]').find('#desccol .description');
						descDiv.text(jQuery(this).find('#editDescText').val());
						if(descDiv.hasClass('muted')) {
							descDiv.toggleClass('muted');
						}
						jq1102(this).modal("hide");
					} else {
						msgDiv.toggleClass('hide alert').text(response);
					}
					jq1102(this).find('#editSubmitButton').button('reset');
				}
			});
		});

});


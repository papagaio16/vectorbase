
(function ($) {
    Drupal.behaviors.displayCharts = {
        attach: function(context, settings) {
            //drupal was sending this variable doubled, so it has to be cut.
/*
            var length = (Drupal.settings.custom_data.orgStats.length)/2;
            var speciesData = Drupal.settings.custom_data.orgStats.splice(length, length);
            var length2 = (Drupal.settings.custom_data.blastNumbers.length)/2;
            var JobTypes = Drupal.settings.custom_data.blastNumbers.splice(length2, length2);
            var length3 = (Drupal.settings.custom_data.jobsDrillDown.length)/2;
            var JobsDrillDown = Drupal.settings.custom_data.jobsDrillDown.splice(length3, length3);
*/


            $('#tool_stats').highcharts({
        chart: {
            type: 'pie',
            events: {
                drilldown: function (e) {
                    UpdateTitle1(e.point.name, ' Total: ' + e.point.y);
                },
                drillup: function (e) {
                    UpdateTitle1('Tool Job Percentages', 'Click the slices to view more statistics');
                }
            }

        },
        title: {
            text: 'Tool Job Percentages'
        },
        subtitle: {
             text: 'Click the slices to view more statistics'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Number of Jobs Ran',
            data: Drupal.settings.custom_data.blastNumbers
        }],
          drilldown: {
          series: Drupal.settings.custom_data.jobsDrillDown
        }
    });
            var chart1 = $('#tool_stats').highcharts();
            function UpdateTitle1(argument1, argument2) {
                chart1.setTitle({
                    text: argument1
                })
                chart1.setTitle(null, { text: argument2
                })
            }

            $('#species').highcharts({

                chart: {
                    type: 'pie',
                    events: {
                        drilldown: function (e) {
                            UpdateTitle(e.point.name, ' Total number of hits: ' + e.point.y);
                        },
                        drillup: function (e) {
                            UpdateTitle('Jobs by Species', 'Click the slices to view more statistics');
                        }
                    }
                },
                title: {
                    text: 'Jobs by Species'
                },
                subtitle: {
                    text: 'Click the slices to view more statistics'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Total Hits Against',
                    data: Drupal.settings.custom_data.orgStats

                }],
                drilldown: {
                    series: Drupal.settings.custom_data.orgStatsDrillDown
                }

            });

            var chart = $('#species').highcharts();

            function UpdateTitle(argument1, argument2) {
                chart.setTitle({
                    text: argument1
                })
                chart.setTitle(null, { text: argument2
                })
            }

            $('#hmm').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Jobs Ran by Date'
                },
                subtitle: {
                    text: 'Click the columns to view Individual Month Statistics.'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                    }
                },
                yAxis: {
                    title: {
                        text: 'Number of Jobs Ran'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
                    //pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [{
                    name: 'Total Jobs Ran',
                    colorByPoint: true,
                    data: Drupal.settings.custom_data.totalDates
                }],
                drilldown: {
                    series: Drupal.settings.custom_data.daysDrillDown
                }
            });


            $('#user_job_stats').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'User/ Anonymous Jobs'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of jobs ran'
                    },
                    stackLabels: {
                        enabled: false,
                        style: {
                            fontWeight: 'bold',
                        //    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -70,
                    verticalAlign: 'top',
                    y: 20,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.point.name + '</b><br/>' +
                            this.series.name + ': ' + this.y;
                    }
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            //color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                //textShadow: '0 0 3px black, 0 0 3px black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'User Jobs',
                    data: Drupal.settings.custom_data.totalDatesUsers,
                    color: '#66FF33',
                    stack: 0
                },
                {
                    name: 'Anonymous Jobs',
                    data: Drupal.settings.custom_data.totalDates,
                    color: '#47B224',
                    stack: 1
                }
                ],
                drilldown: {
                    series: Drupal.settings.custom_data.daysDrillDownCombined

                }
            });






                // create the chart
                $('#stocks').highcharts("StockChart", {
                    chart: {
                        alignTicks: false
                    },

                    rangeSelector: {
                        selected: 1
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Number of jobs ran'
                        },
                    },
                    plotOptions: {
                        area: {
                            fillOpacity:.3
                        }
                    },
                    scrollbar: {
                        barBackgroundColor: 'lightgreen',
                        barBorderWidth: 0,
                        buttonBackgroundColor: 'gray',
                        buttonBorderWidth: 0,
                        buttonArrowColor: 'yellow',
                        rifleColor: 'yellow',
                        trackBackgroundColor: 'white',
                        trackBorderWidth: 1,
                        trackBorderColor: 'silver',
                        trackBorderRadius: 7
                    },
                    navigator: {
                        series: {
                            color: 'lightgreen',
                            maskFill: 'rgba(180, 198, 220, 0.75)'
                        }
                    },
                    title: {
                        text: 'Jobs Ran by Date'
                    },

                    series: [
                        {
                        type: 'line',
                        name: 'User Jobs',
                        data: Drupal.settings.custom_data.stocksJobsUsers,
                        dataGrouping: {
                            units: [[
                                'week', // unit name
                                [1] // allowed multiples
                            ], [
                                'month',
                                [1, 2, 3, 4, 6]
                            ]]
                        }
                    },

                        {
                            type: 'line',
                            name: 'Total Jobs',
                            data: Drupal.settings.custom_data.stocksJobs,
                            dataGrouping: {
                                units: [[
                                    'week', // unit name
                                    [1] // allowed multiples
                                ], [
                                    'month',
                                    [1, 2, 3, 4, 6]
                                ]]
                            }
                        },

                        {
                            type: 'area',
                            name: 'Blast Jobs',
                            data: Drupal.settings.custom_data.blastArea,
                            dataGrouping: {
                                units: [[
                                    'week', // unit name
                                    [1] // allowed multiples
                                ], [
                                    'month',
                                    [1, 2, 3, 4, 6]
                                ]]
                            }
                        },{
                            type: 'area',
                            name: 'Clustal Jobs',
                            data: Drupal.settings.custom_data.clustalArea,
                            dataGrouping: {
                                units: [[
                                    'week', // unit name
                                    [1] // allowed multiples
                                ], [
                                    'month',
                                    [1, 2, 3, 4, 6]
                                ]]
                            }
                        },{
                            type: 'area',
                            name: 'Hmmer Jobs',
                            data: Drupal.settings.custom_data.hmmerArea,
                            dataGrouping: {
                                units: [[
                                    'week', // unit name
                                    [1] // allowed multiples
                                ], [
                                    'month',
                                    [1, 2, 3, 4, 6]
                                ]]
                            }
                        },


                    ]
                });





    }
    };
})(jQuery);

/*
(function ($) {
    Drupal.behaviors.displayCharts1 = {
        attach: function(context, settings) {

            $('#hmm').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Jobs Ran by Date'
                },
                subtitle: {
                    text: 'Click the columns to view Individual Month Statistics.'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                    }
                },
                yAxis: {
                    title: {
                        text: 'Number of Jobs Ran'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
                    //pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [{
                    name: 'Total Jobs Ran',
                    colorByPoint: true,
                    data: Drupal.settings.custom_data.totalDates
                }],
                drilldown: {
                    series: Drupal.settings.custom_data.daysDrillDown
                }
            });



        }
    };
})(jQuery);
*/

/*

(function ($) {
    Drupal.behaviors.displayCharts1 = {
        attach: function(context, settings) {


    Highcharts.data({
        csv: document.getElementById('tsv').innerHTML,
        itemDelimiter: '\t',
        parsed: function (columns) {

            var brands = {},
                brandsData = [],
                versions = {},
                drilldownSeries = [];

            // Parse percentage strings
            columns[1] = $.map(columns[1], function (value) {
                if (value.indexOf('%') === value.length - 1) {
                    value = parseFloat(value);
                }
                return value;
            });

            $.each(columns[0], function (i, name) {
                var brand,
                    version;

                if (i > 0) {

                    // Remove special edition notes
                    name = name.split(' -')[0];

                    // Split into brand and version
                    version = name.match(/([0-9]+[\.0-9x]*)/);
                    if (version) {
                        version = version[0];
                    }
                    brand = name.replace(version, '');

                    // Create the main data
                    if (!brands[brand]) {
                        brands[brand] = columns[1][i];
                    } else {
                        brands[brand] += columns[1][i];
                    }

                    // Create the version data
                    if (version !== null) {
                        if (!versions[brand]) {
                            versions[brand] = [];
                        }
                        versions[brand].push(['v' + version, columns[1][i]]);
                    }
                }

            });

            $.each(brands, function (name, y) {
                brandsData.push({
                    name: name,
                    y: y,
                    drilldown: versions[name] ? name : null
                });
            });
            $.each(versions, function (key, value) {
                drilldownSeries.push({
                    name: key,
                    id: key,
                    data: value
                });
            });

            $.each(drilldownSeries, function(key, value){
                $.each(value, function(key, value){
                    alert(key + value);
                });
            });

            $.each(brandsData, function(key, value){
                $.each(value, function(key, value){
                    alert(key + value);
                });
            });


            // Create the chart
            $('#hmm').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Jobs Ran by Date'
                },
                subtitle: {
                    text: 'Click the columns to view Individual Month Statistics.'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Number of Jobs Ran'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
                    //pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [{
                    name: 'Total Jobs Ran',
                    colorByPoint: true,
                    data: Drupal.settings.custom_data.totalDates
                }],
                drilldown: {
                    series: Drupal.settings.custom_data.daysDrillDown
                }
            });
        }
    });





        }
    };
})(jQuery);
*/
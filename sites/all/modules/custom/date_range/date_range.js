(function ($) {
  $(document).ready(function () {
    //Go through each popup field and add the click event for the done button
    jQuery("[id$=popup-0]").each( function() {
     jQuery('#'+ jQuery(this).attr('id')).focus(function() {
       var thisCalendar = $(this);
       jQuery('.ui-datepicker-calendar').detach();
       jQuery('.ui-datepicker-close').click(function() {
         var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val()
         var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
         thisCalendar.datepicker('setDate', new Date(year, month, 1));
       });
     });
    });
   });
  })(jQuery);

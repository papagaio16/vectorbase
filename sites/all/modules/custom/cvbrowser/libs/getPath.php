<?php

require_once( "dbFunctions.php" );
$db = connectToDb();

$cvId = $db->real_escape_string( $_GET[ 'cvId' ] );
$pathId = $db->real_escape_string( $_GET[ 'pathId' ] );
$termXrefId = $db->real_escape_string( $_GET[ 'termXrefId' ] );


// Find all terms in the path up to the requested term
$q = "SELECT xref_id FROM cv_path, cv_term WHERE cv_path.cv_id='$cvId' AND cv_path.id='$pathId' AND term_xref_id=cv_term.xref_id ORDER BY distance_from_root ASC";

$qr = $db->query( $q );

if( $qr->num_rows == 0 )
{
	exit();
}

$response = "";

$row = $qr->fetch_assoc();

$lastAncestorId = -1;

$searchTermFound = FALSE;

while( $row != FALSE && $searchTermFound == FALSE )
{
	
	$sq = "SELECT DISTINCT(descendant_xref_id), name FROM cv_term_relationship, cv_term WHERE ancestor_xref_id='".$row['xref_id']."' AND descendant_xref_id=xref_id";

	$sqr = $db->query( $sq );
	
	$srow = $sqr->fetch_assoc();
	
	while( $srow != FALSE )
	{
		$response = $response.$row['xref_id']."|".$srow['descendant_xref_id']."|".$srow['name']."|".getChildrenCount( $srow['descendant_xref_id'], $db )."<br>";

		if( $srow['descendant_xref_id'] == $termXrefId )
		{
			$lastAncestorId = $row['xref_id'];
			$searchTermFound = TRUE;
		}
		$srow = $sqr->fetch_assoc();
	}
	
	$row = $qr->fetch_assoc();
}

print $response;



function getChildrenCount( $xrefId, $db )
{
	$q = "SELECT DISTINCT(descendant_xref_id) FROM cv_term_relationship WHERE ancestor_xref_id='$xrefId'";
	$qr = $db->query( $q );
	return $qr->num_rows;
}


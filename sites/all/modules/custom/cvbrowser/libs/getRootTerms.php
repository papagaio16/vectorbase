<?php

require_once( "dbFunctions.php" );
$db = connectToDb();

$cvId = $db->escape_string( $_GET['cvId' ] );

$q = "SELECT root_terms FROM cv WHERE id='$cvId'";
$qr = $db->query( $q );

if( $qr->num_rows == 0 )
{
	exit();
}

$row = $qr->fetch_assoc();

$rootTerms = explode( ",", $row['root_terms'] );
$rootTermsLength = count( $rootTerms );

$response = "";

for( $t = 0; $t < $rootTermsLength; $t++ )
{
	$q = "SELECT name FROM cv_term WHERE xref_id='".$rootTerms[$t]."'";
	$qr = $db->query( $q );
	
	if( $qr->num_rows == 0 )
	{
		exit();
	}
	
	$row = $qr->fetch_assoc();
	$response = $response.$rootTerms[$t]."|".$row['name']."|";
	
	$q = "SELECT id FROM cv_term_relationship WHERE ancestor_xref_id='".$rootTerms[$t]."'";
	$qr = $db->query( $q );
	
	$response = $response . $qr->num_rows . '<br>';
}

print $response;


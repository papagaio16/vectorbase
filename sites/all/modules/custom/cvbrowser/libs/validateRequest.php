<?php

require_once( "dbFunctions.php" );
$db = connectToDb();

$cv = $db->real_escape_string( $_GET[ 'cv' ] );
$termId = $db->real_escape_string( $_GET[ 't' ] );

$q = "SELECT id FROM cv WHERE namespace='$cv'";
$qr = $db->query( $q );

if( $qr->num_rows < 1 )
{
	print "0";
	return;
}

$row = $qr->fetch_assoc();
$cvId = $row[ 'id' ];	

$q = "SELECT xref_id FROM cv_term WHERE cv_id='$cvId' AND id='$termId'";
$qr = $db->query( $q );

if( $qr->num_rows < 1 )
{
	print "0";
	return;
}

$row = $qr->fetch_assoc();
$xrefId = $row[ 'xref_id' ];

$pathId = getPathId( $cvId, $xrefId );

print "[{\"cvId\":$cvId,\"termXrefId\":$xrefId,\"pathId\":$pathId}]";


<?php

function connectToDb()
{
	$db = new mysqli( "localhost", "cv_admin", "cV.Br0wS3R!", "cvbrowser" );
	if( $db->connect_errno )
	{
		exit( "Database connection error: $db->connect_error" );
	}
	
	return $db;
}


function printDbError( $q )
{
	global $db;

	print "\nDatabase error! ". $db->affected_rows."\n";
	print $db->connect_error."\n";
	print $q."\n";
	exit( 0 );
}


function getDbTermIdByTermId( $termId )
{
    global $db;

	$q = "SELECT id FROM cv_term WHERE term_id='$termId'";
	$qr = $db->query( $q );
	
	$matchingRows = $qr->num_rows;
	
	if( $matchingRows < 1 || $matchingRows > 1 )
	{
		dbAlert( $q );
	}
	
	$row = $qr->fetch_assoc();
	
	return $row[ 'id' ];
}


function getTermIdByDbTermId( $dbTermId )
{

    global $db;

	$q = "SELECT term_id FROM cv_term WHERE id='$dbTermId'";
	$qr = $db->query( $q );

	$matchingRows = $qr->num_rows;

	if( $matchingRows < 1 || $matchingRows > 1 )
	{
		dbAlert( $q );
    	}

	$row = $qr->fetch_assoc();

	return $row[ 'term_id' ];
}


function getPathId( $cvId, $xrefId )
{

    global $db;

	$q = "SELECT id FROM cv_path WHERE cv_id=$cvId AND term_xref_id='$xrefId' LIMIT 1";	
	$qr = $db->query( $q );

	if( $qr->num_rows > 0 )
	{
		$row = $qr->fetch_assoc();
		return $row['id'];
	}
	else
	{
		return "-1";
	}
}


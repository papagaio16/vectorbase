<?php

require_once( "dbFunctions.php" );
$db = connectToDb();

$cvId = $db->real_escape_string( $_GET[ 'id' ] );

$q = "SELECT * FROM cv WHERE id=$cvId AND state=1";
$qr = $db->query( $q );

if( $qr->num_rows < 1 )
{
	return;
}

$row = $qr->fetch_assoc();

print $row[ 'browsing_mode' ]."÷";

print $row[ 'terms' ]."÷";

$q = "SELECT xref_id FROM cv_term WHERE cv_id='$cvId' AND root=1 ORDER BY name ASC";
$qr = $db->query( $q );

$rootTermsCount = $qr->num_rows;

$n = 0;
for( $n = 0; $n < $rootTermsCount; $n++)
{
	if( $n > 0 )
	{
		print "¸";
	}

	$row = $qr->fetch_assoc();
	
	print $row[ 'xref_id' ];
}


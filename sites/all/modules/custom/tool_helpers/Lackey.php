<?php
namespace VectorBase\ToolHelpers;

abstract class Lackey {
    
    protected $myId;
            
    function __construct($id = '') {
	if (!$id) {
            $this->myId = db_query("SELECT nextval('job_id_sequence')")->fetchObject()->nextval;
        }
        else {
            $this->myId = $id;
        }
    }
    
    public function getId() {
        return $this->myId;
    }
    
    public function submit(JobSpec $job_specification) {
        
        $mySubmitCommands = $this->buildSubmitFiles($job_specification, $this->myId);
        //$exec_out = array('submitted to cluster ' . db_query("SELECT nextval('fake_condor_ids')")->fetchObject()->nextval . ".0");
        $exec_out = array();
        exec($mySubmitCommands[0] . " " . $mySubmitCommands[1], $exec_out); 
        
        #parse out condor job id from the $exec_out
        $myJobIds = $this->lackey_parseJobId($exec_out);
        $this->lackey_StoreJobParamsInDB($job_specification, $myJobIds);
        
        return $this->myId;
    }
    
    abstract protected function buildSubmitFiles(JobSpec $job_specification, $prefix);
    
    private function lackey_parseJobId($exec_str) {
        $jobIds = "";
	foreach ($exec_str as $line) {
            $matches = array();
            preg_match('/submitted to cluster (\d+)/', $line, $matches);
            if($matches[1]){
		if ($jobIds) {
			$jobIds .= ",$matches[1]";
		}
		else {
			$jobIds = $matches[1];
		}
            }
        }
	return $jobIds;
    }
    
    private function lackey_StoreJobParamsInDB(JobSpec $job, $jobIds){
        //save some job params in the db
        global $user;
        $args = array();
        $args['sequence'] = $job->get_input();
        $args['user_name']=$user->name;
        $args['submitter_ip']=$_SERVER['REMOTE_ADDR'];
        $args['detailed_est']='F';
        $args['program']=$job->get_program();
        $args['date']=date("m/d/y");
        $args['time']=date("H:i:s");
        $args['rawId']=$jobIds;
        $args['description'] = $job->get_description();
        $args['condor_job_id'] = $jobIds;
        
        foreach($job->get_attributes() as $attrib => $value) {
            $args[$attrib] = $value;
        }
        $this->lackey_save_params($jobIds,$args);
        
        #database information        
        foreach($job->get_target_dbs() as $db){
            $params['target_database']=$db;
            $this->lackey_save_params($jobIds,$params);
        }
        
    }
    
    private function lackey_save_params($id, $args) {
        
        foreach($args as $param => $value){
            $fields = array('job_id' => $this->myId, 'argument' => $param, 'value' => $value);
            db_insert('xgrid_job_params')->fields($fields)->execute();
            //dpm("Inserting $param with a value of $value for job id $this->myId"); // Pretty sure this works! just don't want to execute anything just yet
        }
    }
}

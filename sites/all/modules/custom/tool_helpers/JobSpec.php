<?php
namespace VectorBase\ToolHelpers;

class JobSpec {
    private $program;
    private $input;
    private $attributes;
    private $target_databases;
    private $description;

    function __construct($prog, $str_input, $attrib_map, $db_list=array(), $desc='') {
        $this->program = $prog;
        $this->input = $str_input;
        $this->attributes = $attrib_map;
        $this->target_databases = $db_list;
        $this->description = $desc;
        
    }
    
    public function get_program() {
        return $this->program;
    }

    public function set_program($str_program) {
        $this->program = $str_program;
    }
    
    public function get_input() {
        return $this->input;
    }
    
    public function set_input($str_input) {
        $this->input = $str_input;
    }
    
    public function get_attributes() {
        return $this->attributes;
    }
    
    public function set_attributes($attrib_map) {
        $this->attributes = $attrib_map;
    }
    
    public function get_target_dbs() {
        return $this->target_databases;
    }
    
    public function set_target_dbs($db_list) {
        $this->target_databases = $db_list;
    }
    
    public function get_description() {
        return $this->description;
    }
    
    public function set_description($desc) {
        $this->description = $desc;
    }
}
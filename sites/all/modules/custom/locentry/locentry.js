var map;
var marker;
var myCircle;

function radiustoZoom(radius){
      	return Math.round(14-Math.log(radius)/Math.LN2)-1;
}

function mapinitialize() {
	var myzoom = radiustoZoom(20);
	var myLatlng = new google.maps.LatLng(41.6833333,-86.25);
	var coords = document.getElementById('edit-field-search-geo-location-und-0-coords');
	var radius = document.getElementById('edit-field-search-geo-location-und-0-radius');
	var mapOptions = {
	center: myLatlng,
	zoom: myzoom, //Max 21
	scaleControl: true
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	marker = new google.maps.Marker({
	position: myLatlng,
	map: map,
	title: 'Selected center'
	});
        myCircle = new google.maps.Circle({
		map:map, 
                center:myLatlng,
                radius:radius.value * 1000,
                strokeColor:"0000FF", 
                strokeOpacity:0.8,
                strokeWeight:2
         });     

	if(coords.value.length > 0 && radius.value.length > 0){
		//alert('Resized');
		mapresize();
	}
}

function mapresize() {
	var coords = document.getElementById('edit-field-search-geo-location-und-0-coords');
	var radius = document.getElementById('edit-field-search-geo-location-und-0-radius');
	var myzoom;

	if(radius.value > 0){
		myzoom = radiustoZoom(radius.value);
	}else{
		alert('Warning - invalid radius');
		myzoom = map.zoom;
	}

	var center = centervalue(coords.value);
	google.maps.event.trigger(map, 'resize');
	map.setZoom(myzoom);
	map.setCenter(center);	
	marker.setPosition(center);
	myCircle.setCenter(center);
	myCircle.setRadius(radius.value * 1000);
	//alert(center);
}

function centervalue(coords){
	var mycenter;
	//alert(coords.search(','));
	if((coords.length > 0) && (coords.search(',') > 0)){
		var coordarray = coords.split(',',2);
		var lat = coordarray[0].trim();
		var lon = coordarray[1].trim();
		if(((lat >= -90) && (lat <=90)) && ((lon >= -180) && (lon <= 180))){
			mycenter = new google.maps.LatLng(lat,lon);
		}else{
			alert('Warning - invalid coordinates');
			mycenter = map.center;
		}
	}else{
		mycenter = map.center;  
	}
	return mycenter;
}

//google.maps.event.addDomListener(window, 'load', mapinitialize);

//google.maps.event.addDomListener(window, 'change', mapresize);

